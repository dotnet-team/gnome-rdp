// 
//  ResourceLoader.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Collections.Generic;

using Gdk;

namespace GnomeRDP
{
	public static class ResourceLoader
	{
		private static readonly Dictionary<string, Pixbuf> pixBufs = new Dictionary<string, Pixbuf>();
		
		public static class Icons
		{
			public const string gnomeRdp = "GnomeRDP.Resources.gnome-rdp-icon.png";
			public const string folderSmall = "GnomeRDP.Resources.group_16.png";
			
			public const string rdpSmall = "GnomeRDP.Resources.rdp_16.png";
			public const string rdp = "GnomeRDP.Resources.rdp.png";
			
			public const string sshSmall = "GnomeRDP.Resources.ssh_16.png";
			public const string ssh = "GnomeRDP.Resources.ssh.png";
			
			public const string vncSmall = "GnomeRDP.Resources.vnc_16.png";
			public const string vnc = "GnomeRDP.Resources.vnc.png";
		}
		
		public static Pixbuf Find(string name)
		{
			Pixbuf value;
			lock(pixBufs)
			{
				if (pixBufs.TryGetValue(name, out value) == false)
				{
					value = Pixbuf.LoadFromResource(name);
					pixBufs[name] = value;
				}
			}
			return value;
		}
	}
}
