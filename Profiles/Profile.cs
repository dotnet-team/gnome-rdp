// 
//  Profile.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Collections.Generic;

using Gdk;

using GnomeRDP.Rdp;
using GnomeRDP.Vnc;
using GnomeRDP.Ssh;

namespace GnomeRDP.Profiles
{
	public abstract class Profile : IIdType, IEquatable<Profile>, ISimiliar<Profile>
	{
		public Profile(Protocol protocol, string id, string description)
		{
			this.protocol = protocol;
			this.id = id;
			this.description = description;
		}
		
		public Profile(Protocol protocol, Dictionary<string, string> dictionary)
		{
			this.protocol = protocol;
			this.id = dictionary[Fields.id];
			this.description = dictionary[Fields.description];
		}
		
		protected static class Fields
		{
			public const string protocol = "Protocol";
			public const string id = "Id";
			public const string description = "Description";
		}

		protected readonly Protocol protocol;
		public Protocol Protocol
		{
			get
			{
				return protocol;
			}
		}		
				
		private readonly string id;
		public string Id
		{
			get
			{
				return id;
			}
		}
		
		private string description;
		public string Description
		{
			get
			{
				return description;
			}			
			set
			{
				description = value;
			}
		}
		
		public static Pixbuf GetIcon(Profile item, bool small)
		{
			if (item == null) return null;
			
			switch (item.Protocol)
			{
				case Protocol.rdp: return ResourceLoader.Find(small ? ResourceLoader.Icons.rdpSmall: ResourceLoader.Icons.rdp);
				case Protocol.ssh: return ResourceLoader.Find(small ? ResourceLoader.Icons.sshSmall: ResourceLoader.Icons.ssh);
				case Protocol.vnc: return ResourceLoader.Find(small ? ResourceLoader.Icons.vncSmall: ResourceLoader.Icons.vnc);
				default: return null;
			}
		}
		
		public Pixbuf GetIcon(bool small)
		{
			return GetIcon(this, small);
		}

		public bool Equals(Profile other)
		{
			return id == other.id;
		}
					
		public abstract bool IsLike(Profile other);
	}
}
