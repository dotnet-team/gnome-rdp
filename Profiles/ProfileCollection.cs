// 
//  ProfileCollection.cs
//  
//  Author:
//       jmichels <${AuthorEmail}>
// 
//  Copyright (c) 2010 jmichels
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Collections;
using System.Collections.Generic;

using GnomeRDP.ApplicationDataFiles;
using GnomeRDP.Rdp;
using GnomeRDP.Vnc;
using GnomeRDP.Ssh;

namespace GnomeRDP.Profiles
{
	public class ProfileCollection : Collection<Profile>
	{
		protected override Profile[] LoadItems()
		{
			var items = new List<Profile>();
			
			items.AddRange(FileManager.LoadRdpProfiles());
			items.AddRange(FileManager.LoadVncProfiles());
			items.AddRange(FileManager.LoadSshProfiles());
			
			return items.ToArray();
		}

		public void Save(Protocol protocol)
		{
			switch (protocol) 
			{
				case Protocol.rdp: FileManager.SaveRdpProfiles(GetProfiles<RdpProfile>()); break;
				case Protocol.vnc: FileManager.SaveVncProfiles(GetProfiles<VncProfile>()); break;
				case Protocol.ssh: FileManager.SaveSshProfiles(GetProfiles<SshProfile>()); break;
				default: throw new ArgumentException(string.Format("Unknown protocol {0}", protocol));
			}
		}
		
		public override void Save ()
		{
			FileManager.SaveRdpProfiles(GetProfiles<RdpProfile>());
			FileManager.SaveVncProfiles(GetProfiles<VncProfile>());
			FileManager.SaveSshProfiles(GetProfiles<SshProfile>());
		}

		public IEnumerable<U> GetProfiles<U>() where U : Profile
		{
			foreach (var item in Items) 
			{
				if (item is U) yield return (U)item;
			}
		}	
	}
}
