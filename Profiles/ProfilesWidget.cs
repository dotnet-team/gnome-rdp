// 
//  ProfilesWidget.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Collections.Generic;
using System.ComponentModel;	

using Gtk;	
using Gdk;

using GnomeRDP.Identies;
using GnomeRDP.Sessions;
using GnomeRDP.Logging;
using GnomeRDP.Rdp;
using GnomeRDP.Ssh;
using GnomeRDP.Vnc;

namespace GnomeRDP.Profiles
{

	[ToolboxItem(true)]
	public partial class ProfilesWidget : Bin
	{
		private Menu menu = new Menu();
						
		public ProfilesWidget ()
		{
			this.Build ();
			
			Initialize();		
		}

		private static class Columns
		{
			public const int id = 0;
			public const int icon = 1;
			public const int protocol = 2;
			public const int description = 3;
		}
		
		private void Initialize()
		{			
			treeView.AppendColumn(new TreeViewColumn(null, new CellRendererPixbuf(), "pixbuf", Columns.icon));
			treeView.AppendColumn(new TreeViewColumn("Protocol", new CellRendererText(), "text", Columns.protocol));
			treeView.AppendColumn(new TreeViewColumn("Description", new CellRendererText(), "text", Columns.description));
			treeView.Selection.Mode = SelectionMode.Multiple;
			treeView.Model = new ListStore(typeof(string), typeof(Pixbuf), typeof(string), typeof(string));
			treeView.RulesHint = true;
		
			Resync();
			
			// Initialize Context Menu
			var propertiesMenuItem = new MenuItem("Properties");
			propertiesMenuItem.Activated += OnMenuPropertiesActivated;
			menu.Add(propertiesMenuItem);

			var deleteMenuItem = new MenuItem("Delete");
			deleteMenuItem.Activated += OnMenuDeleteActivated;			
			menu.Add(deleteMenuItem);		
		}

		private void Resync()
		{
			try
			{
				ListStore listStore = (ListStore)treeView.Model;			
				listStore.Clear();
				
				foreach (var item in Program.ProfileCollection.Items) 
				{
					listStore.AppendValues(item.Id, item.GetIcon(true), item.Protocol.ToString(), item.Description);
				}				
			}
			catch
			{
			}
		}

		private Profile[] GetSelectedProfiles()
		{
			var items = new List<Profile>();
			try
			{
				foreach (var path in treeView.Selection.GetSelectedRows()) 
				{
					TreeIter iter;
					treeView.Model.GetIter(out iter, path);
								
					string id = (string)treeView.Model.GetValue(iter, Columns.id);
					items.Add(Program.ProfileCollection.Find(id));
				}
				return items.ToArray();
			}
			catch
			{
				return new Profile[0];
			}
		}
		
		[GLib.ConnectBefore()]
		protected virtual void OnTreeViewButtonPressEvent (object o, Gtk.ButtonPressEventArgs args)
		{
			try
			{	
				if (args.Event.Button != 3) 
				{
					args.RetVal = false;
					return;
				}
				
				TreePath path;
				if (treeView.GetPathAtPos((int)args.Event.X, (int)args.Event.Y, out path) == false) return;
				
				treeView.GrabFocus();
				treeView.SetCursor(path, treeView.Columns[0], false);
				
				
				menu.ShowAll();
				menu.Popup();
				
				args.RetVal = true;
			}
			catch
			{
			}
		}

		protected virtual void OnMenuPropertiesActivated (object sender, System.EventArgs e)
		{
			try
			{
				foreach (var profile in GetSelectedProfiles()) 
				{
					bool result;
						
					switch (profile.Protocol)
					{
						case Protocol.rdp: result = EditRdpProfile((RdpProfile)profile); break;
						case Protocol.vnc: result = EditVncProfile((VncProfile)profile); break;
						case Protocol.ssh: result = EditSshProfile((SshProfile)profile); break;
						default: result = false; break;							
					}
						
					if (result == true) Program.ProfileCollection.Save(profile.Protocol);
				}
			}
			catch(Exception ex)
			{
				Log.Add(ex);
			}

			Resync();
		}
		
		private bool EditRdpProfile(RdpProfile profile)
		{
			RdpProfileDialog dlg = new RdpProfileDialog(profile);
			try
			{
				if (dlg.Run() != (int)ResponseType.Ok) return false;
					
				profile.Description = dlg.Description;
				profile.RdpVersion = dlg.RdpVersion;
				profile.KeyboardLayout = dlg.KeyboardLayout;
				profile.Width = dlg.RdpWidth;
				profile.Height = dlg.RdpHeight;
				profile.RdpColorDepth = dlg.RdpColorDepth;
				profile.RdpExperience = dlg.RdpExperience;
				profile.RdpSound = dlg.RdpSound;
				profile.FullScreen = dlg.FullScreen;
				profile.EnableRdpCompression = dlg.EnableCompression;
				profile.AttachToConsole = dlg.AttachToConsole;
					
				return true;
			}
			catch(Exception ex)
			{
				Log.Add(ex);
				return false;
			}
			finally
			{
				dlg.Destroy();
			}			
		}
		
		private bool EditVncProfile(VncProfile profile)
		{
			VncProfileDialog dlg = new VncProfileDialog(profile);
			try
			{
				if (dlg.Run() != (int)ResponseType.Ok) return false;
					
				profile.Description = dlg.Description;
				profile.ColorDepth = dlg.ColorDepth;
				profile.Encoding = dlg.Encoding;
				profile.FullScreen = dlg.FullScreen;
				profile.Shared = dlg.Shared;
				profile.ViewOnly = dlg.ViewOnly;
				
				return true;
			}
			catch(Exception ex)
			{
				Log.Add(ex);
				return false;
			}
			finally
			{
				dlg.Destroy();
			}			
		}
		
		private bool EditSshProfile(SshProfile profile)
		{
			SshProfileDialog dlg = new SshProfileDialog(profile);
			try
			{
				if (dlg.Run() != (int)ResponseType.Ok) return false;
					
				profile.Description = dlg.Description;
				profile.FullScreen = dlg.FullScreen;
				profile.X11Forwarding = dlg.X11Forwarding;
					
				return true;
			}
			catch(Exception ex)
			{
				Log.Add(ex);
				return false;
			}
			finally
			{
				dlg.Destroy();
			}			
		}
		
		protected virtual void OnMenuDeleteActivated (object sender, System.EventArgs e)
		{
			try
			{
				foreach (var profile in GetSelectedProfiles()) 
				{
					MessageDialog dlg = new MessageDialog(null, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, "Are you sure you want to delete {0} profile {1}?", profile.Protocol, profile.Description);
					try
					{
						if (dlg.Run() != (int)ResponseType.Yes) continue;
					}
					finally
					{
						dlg.Destroy();
					}
						
					Program.ProfileCollection.Remove(profile);
				}
			}
			catch(Exception ex)
			{
				Log.Add(ex);
			}

			Resync();
		}

		protected virtual void OnNewRdpActionActivated (object sender, System.EventArgs e)
		{
			try
			{
				RdpProfile profile = new RdpProfile();
				if (EditRdpProfile(profile)) Program.ProfileCollection.Add(profile);
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}		
			
			Resync();			
		}
		
		protected virtual void OnNewVncActionActivated (object sender, System.EventArgs e)
		{
			try
			{
				VncProfile profile = new VncProfile();
				if (EditVncProfile(profile)) Program.ProfileCollection.Add(profile);
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
			
			Resync();
		}
		
		protected virtual void OnNewSshActionActivated (object sender, System.EventArgs e)
		{
			try
			{
				SshProfile profile = new SshProfile();
				if (EditSshProfile(profile)) Program.ProfileCollection.Add(profile);
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
			
			Resync();
		}
	}
}
