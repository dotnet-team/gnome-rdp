// 
//  VncProfileDialog.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2010 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;

using Gtk;

namespace GnomeRDP.Vnc
{
	public partial class VncProfileDialog : Gtk.Dialog
	{
		public VncProfileDialog (VncProfile profile)
		{
			this.Build ();

			string colorDepthName = (profile == null) ? null : profile.ColorDepth.ToString();
			ListStore colorDepthModel = (ListStore)comboboxColorDepth.Model;
			foreach(var name in Enum.GetNames(typeof(VncColorDepth)))
			{
				var iter = colorDepthModel.AppendValues(name);
				if (name == colorDepthName) comboboxColorDepth.SetActiveIter(iter);
			}
					
			string encodingName = (profile == null) ? null : profile.Encoding.ToString();
			ListStore encodingModel = (ListStore)comboboxEncoding.Model;
			foreach (var name in Enum.GetNames(typeof(VncEncoding))) 
			{
				var iter = encodingModel.AppendValues(name);
				if (name == encodingName) comboboxEncoding.SetActiveIter(iter);
			}
			
			if (profile == null) return;

			entryDescription.Text = profile.Description;
			
			checkbuttonFullScreen.Active = profile.FullScreen;
			checkbuttonViewOnly.Active = profile.ViewOnly;
			checkbuttonShared.Active = profile.Shared;
		}
		
		public string Description
		{
			get
			{
				return entryDescription.Text;
			}
		}
				
		public VncColorDepth ColorDepth
		{
			get
			{
				TreeIter iter;
				if (comboboxColorDepth.GetActiveIter(out iter) == false) throw new InvalidOperationException("No color depth is selected");
				
				ListStore model = (ListStore)comboboxColorDepth.Model;
				string name = (string)model.GetValue(iter, 0);
				
				return (VncColorDepth)Enum.Parse(typeof(VncColorDepth), name);				
			}
		}
		
		public VncEncoding Encoding
		{
			get
			{
				TreeIter iter;
				if (comboboxEncoding.GetActiveIter(out iter) == false) throw new InvalidOperationException("No encoding is selected");
				
				ListStore model = (ListStore)comboboxEncoding.Model;
				string name = (string)model.GetValue(iter, 0);
				
				return (VncEncoding)Enum.Parse(typeof(VncEncoding), name);
			}
		}
		
		public bool FullScreen
		{
			get
			{
				return checkbuttonFullScreen.Active;
			}
		}
		
		public bool ViewOnly
		{
			get
			{
				return checkbuttonViewOnly.Active;
			}
		}
		
		public bool Shared
		{
			get
			{
				return checkbuttonShared.Active;
			}
		}
	}
}
