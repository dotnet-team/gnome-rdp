// 
//  VncProfile.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Text;
using System.Collections.Generic;

using GnomeRDP.Profiles;

using GnomeRDP.Identies;

namespace GnomeRDP.Vnc
{
	public class VncProfile : Profile
	{
		public VncProfile () : base(Protocol.vnc, Program.CreateID(), "")
		{
		}
		
		private VncProfile(Dictionary<string, string> dictionary)
			: base(Protocol.vnc, dictionary)
		{
			fullScreen = bool.Parse(dictionary[Fields.fullScreen]);
			viewOnly = bool.Parse(dictionary[Fields.viewOnly]);
			shared = bool.Parse(dictionary[Fields.shared]);
			colorDepth = (VncColorDepth)Enum.Parse(typeof(VncColorDepth), dictionary[Fields.colorDepth]);
			encoding = (VncEncoding)Enum.Parse(typeof(VncEncoding), dictionary[Fields.encoding]);
		}

		private new static class Fields
		{
			public const string fullScreen = "FullScreen";
			public const string viewOnly = "ViewOnly";
			public const string shared = "Shared";
			public const string colorDepth = "ColorDepth";
			public const string encoding = "Encoding";
		}
	
		public static VncProfile Create(Dictionary<string, string> dictionary)
		{
			return new VncProfile(dictionary);
		}

		public static Dictionary<string, string> ToDictionary(VncProfile item)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			
			dictionary[Profile.Fields.id] = item.Id;
			dictionary[Profile.Fields.description] = item.Description;
			
			dictionary[Fields.fullScreen] = item.FullScreen.ToString();
			dictionary[Fields.viewOnly] = item.ViewOnly.ToString();
			dictionary[Fields.shared] = item.Shared.ToString();
			dictionary[Fields.colorDepth] = item.ColorDepth.ToString();
			dictionary[Fields.encoding] = item.Encoding.ToString();
						
			return dictionary;
		}
				
		private bool fullScreen = false;
		public bool FullScreen
		{
			get
			{
				return fullScreen;
			}
			set
			{
				fullScreen = value;
			}
		}		

		private bool viewOnly = false;
		public bool ViewOnly
		{
			get
			{
				return viewOnly;
			}
			set
			{
				viewOnly = value;
			}
		}
		
		private bool shared = false;
		public bool Shared
		{
			get
			{
				return shared;
			}
			set
			{
				shared = value;
			}
		}
		
		private VncColorDepth colorDepth;
		public VncColorDepth ColorDepth
		{
			get
			{
				return colorDepth;
			}
			set
			{
				colorDepth = value;
			}
		}
		
		private VncEncoding encoding;
		public VncEncoding Encoding
		{
			get
			{
				return encoding;
			}
			set
			{
				encoding = value;
			}
		}
		
		public override bool IsLike (Profile other)
		{
			var vncOther = other as VncProfile;
			if (vncOther == null) return false;
			
			return FullScreen == vncOther.FullScreen
				&& ViewOnly == vncOther.ViewOnly
				&& Shared == vncOther.Shared
				&& ColorDepth == vncOther.ColorDepth
				&& Encoding == vncOther.Encoding;
		}
		
		public string ToCommandLineArguements(string server, Identity identity, bool hidePassword)
		{
			StringBuilder text = new StringBuilder();
			
			string user = (identity == null) ? null : identity.Username;
			if (string.IsNullOrEmpty(user) == false)
			{
				if (user.Contains(" "))
				{
					text.AppendFormat(" -user \"{0}\"", user);
				}
				else
				{
					text.AppendFormat(" -user {0}", user);
				}
			}
			
			if (FullScreen) text.Append(" -fullscreen");
			if (ViewOnly) text.Append(" -viewonly");
			if (Shared == false) text.Append(" -noshared");
			
			text.AppendFormat(" -depth {0}", (byte)ColorDepth);
			
			var encoding = Encoding;
			if (encoding != VncEncoding.Unspecified)
			{
				text.AppendFormat(" -encodings {0}",  encoding.ToString().ToLower());
			}
			
			return text.ToString();
		}
	}
}