// 
//  SshProfile.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using GnomeRDP.Profiles;
using GnomeRDP.Identies;

namespace GnomeRDP.Ssh
{
	public class SshProfile : Profile
	{
		public SshProfile () : base(Protocol.ssh, Program.CreateID(), "")
		{
		}
		
		private SshProfile(Dictionary<string, string> dictionary)
			: base(Protocol.ssh, dictionary)
		{
			fullScreen = bool.Parse(dictionary[Fields.fullScreen]);
			x11Forwarding = bool.Parse(dictionary[Fields.x11Forwarding]);
		}

		private new static class Fields
		{
			public const string fullScreen = "FullScreen";
			public const string x11Forwarding = "X11Forwarding"; 
		}
	
		public static SshProfile Create(Dictionary<string, string> dictionary)
		{
			return new SshProfile(dictionary);
		}

		public static Dictionary<string, string> ToDictionary(SshProfile item)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			
			dictionary[Profile.Fields.id] = item.Id;
			dictionary[Profile.Fields.description] = item.Description;
			
			dictionary[Fields.fullScreen] = item.FullScreen.ToString();
			dictionary[Fields.x11Forwarding] = item.X11Forwarding.ToString();
			
			return dictionary;
		}
		
		private bool fullScreen = false;
		public bool FullScreen
		{
			get
			{
				return fullScreen;
			}
			set
			{
				fullScreen = value;
			}
		}		

		private bool x11Forwarding = false;
		public bool X11Forwarding
		{
			get
			{
				return x11Forwarding;
			}
			set
			{
				x11Forwarding = value;
			}
		}
				
		public override bool IsLike (Profile other)
		{
			var sshOther = other as SshProfile;
			if(sshOther == null) return false;
			
			return FullScreen == sshOther.FullScreen
				&& X11Forwarding == sshOther.X11Forwarding;
		}
		
		public string ToCommandLineArguements(string server, Identity identity)
		{
			StringBuilder text = new StringBuilder();

			if (identity != null && string.IsNullOrEmpty(identity.Username) ==false)
			{
				text.AppendFormat(" -l {0}", identity.Username);
			}
			
			if (X11Forwarding) text.AppendFormat(" -X");
			
			text.Append(" -e none -t");
			
			text.AppendFormat(" {0}", server);
			
			return text.ToString();
		}
	}
}
