// 
//  SshProfileDialog.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2010 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;

using GnomeRDP.Ssh;

namespace GnomeRDP
{
	public partial class SshProfileDialog : Gtk.Dialog
	{
		public SshProfileDialog (SshProfile profile)
		{
			this.Build ();
			
			txtDescription.Text = profile.Description;
			chkFullScreen.Active = profile.FullScreen;
			chkX11Forwarding.Active = profile.X11Forwarding;
		}

		public string Description
		{
			get
			{
				return txtDescription.Text;
			}
		}
		
		public bool FullScreen
		{
			get
			{
				return chkFullScreen.Active;
			}
		}
		
		public bool X11Forwarding
		{
			get
			{
				return chkX11Forwarding.Active;
			}
		}
	}
}
