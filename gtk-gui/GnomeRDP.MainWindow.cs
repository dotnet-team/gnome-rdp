
// This file has been generated by the GUI designer. Do not modify.
namespace GnomeRDP
{
	public partial class MainWindow
	{
		private global::Gtk.UIManager UIManager;

		private global::Gtk.VBox mainWindowContainer;

		private global::Gtk.Notebook notebook;

		private global::GnomeRDP.Sessions.SessionsWidget sessionswidget1;

		private global::Gtk.Label sessionsLabel;

		private global::GnomeRDP.Identies.IdentitiesWidget identitieswidget1;

		private global::Gtk.Label identitiesLabel;

		private global::GnomeRDP.Profiles.ProfilesWidget profileswidget1;

		private global::Gtk.Label profilesLabel;

		private global::GnomeRDP.LogWidget logwidget1;

		private global::Gtk.Label Loglabel;

		protected virtual void Build ()
		{
			global::Stetic.Gui.Initialize (this);
			// Widget GnomeRDP.MainWindow
			this.UIManager = new global::Gtk.UIManager ();
			global::Gtk.ActionGroup w1 = new global::Gtk.ActionGroup ("Default");
			this.UIManager.InsertActionGroup (w1, 0);
			this.AddAccelGroup (this.UIManager.AccelGroup);
			this.Events = ((global::Gdk.EventMask)(256));
			this.Name = "GnomeRDP.MainWindow";
			this.Title = global::Mono.Unix.Catalog.GetString ("Gnome RDP");
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			// Container child GnomeRDP.MainWindow.Gtk.Container+ContainerChild
			this.mainWindowContainer = new global::Gtk.VBox ();
			this.mainWindowContainer.Name = "mainWindowContainer";
			// Container child mainWindowContainer.Gtk.Box+BoxChild
			this.notebook = new global::Gtk.Notebook ();
			this.notebook.CanFocus = true;
			this.notebook.Name = "notebook";
			this.notebook.CurrentPage = 1;
			this.notebook.TabPos = ((global::Gtk.PositionType)(0));
			// Container child notebook.Gtk.Notebook+NotebookChild
			this.sessionswidget1 = new global::GnomeRDP.Sessions.SessionsWidget ();
			this.sessionswidget1.Events = ((global::Gdk.EventMask)(256));
			this.sessionswidget1.Name = "sessionswidget1";
			this.notebook.Add (this.sessionswidget1);
			// Notebook tab
			this.sessionsLabel = new global::Gtk.Label ();
			this.sessionsLabel.Name = "sessionsLabel";
			this.sessionsLabel.LabelProp = global::Mono.Unix.Catalog.GetString ("Sessions");
			this.notebook.SetTabLabel (this.sessionswidget1, this.sessionsLabel);
			this.sessionsLabel.ShowAll ();
			// Container child notebook.Gtk.Notebook+NotebookChild
			this.identitieswidget1 = new global::GnomeRDP.Identies.IdentitiesWidget ();
			this.identitieswidget1.Events = ((global::Gdk.EventMask)(256));
			this.identitieswidget1.Name = "identitieswidget1";
			this.notebook.Add (this.identitieswidget1);
			global::Gtk.Notebook.NotebookChild w3 = ((global::Gtk.Notebook.NotebookChild)(this.notebook[this.identitieswidget1]));
			w3.Position = 1;
			// Notebook tab
			this.identitiesLabel = new global::Gtk.Label ();
			this.identitiesLabel.Name = "identitiesLabel";
			this.identitiesLabel.LabelProp = global::Mono.Unix.Catalog.GetString ("Identities");
			this.notebook.SetTabLabel (this.identitieswidget1, this.identitiesLabel);
			this.identitiesLabel.ShowAll ();
			// Container child notebook.Gtk.Notebook+NotebookChild
			this.profileswidget1 = new global::GnomeRDP.Profiles.ProfilesWidget ();
			this.profileswidget1.Events = ((global::Gdk.EventMask)(256));
			this.profileswidget1.Name = "profileswidget1";
			this.notebook.Add (this.profileswidget1);
			global::Gtk.Notebook.NotebookChild w4 = ((global::Gtk.Notebook.NotebookChild)(this.notebook[this.profileswidget1]));
			w4.Position = 2;
			// Notebook tab
			this.profilesLabel = new global::Gtk.Label ();
			this.profilesLabel.Name = "profilesLabel";
			this.profilesLabel.LabelProp = global::Mono.Unix.Catalog.GetString ("Profiles");
			this.notebook.SetTabLabel (this.profileswidget1, this.profilesLabel);
			this.profilesLabel.ShowAll ();
			// Container child notebook.Gtk.Notebook+NotebookChild
			this.logwidget1 = new global::GnomeRDP.LogWidget ();
			this.logwidget1.Events = ((global::Gdk.EventMask)(256));
			this.logwidget1.Name = "logwidget1";
			this.notebook.Add (this.logwidget1);
			global::Gtk.Notebook.NotebookChild w5 = ((global::Gtk.Notebook.NotebookChild)(this.notebook[this.logwidget1]));
			w5.Position = 3;
			// Notebook tab
			this.Loglabel = new global::Gtk.Label ();
			this.Loglabel.Name = "Loglabel";
			this.Loglabel.LabelProp = global::Mono.Unix.Catalog.GetString ("Log");
			this.notebook.SetTabLabel (this.logwidget1, this.Loglabel);
			this.Loglabel.ShowAll ();
			this.mainWindowContainer.Add (this.notebook);
			global::Gtk.Box.BoxChild w6 = ((global::Gtk.Box.BoxChild)(this.mainWindowContainer[this.notebook]));
			w6.Position = 0;
			this.Add (this.mainWindowContainer);
			if ((this.Child != null)) {
				this.Child.ShowAll ();
			}
			this.DefaultWidth = 798;
			this.DefaultHeight = 565;
			this.Show ();
		}
	}
}
