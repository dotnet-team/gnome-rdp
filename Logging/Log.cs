//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.Diagnostics;

namespace GnomeRDP.Logging
{
	public static class Log
	{
		private static LogLevel logLevelFilter = LogLevel.Information;
		public static LogLevel LogLevelFilter
		{
			get
			{
				return logLevelFilter;
			}
			set
			{
				logLevelFilter = value;
			}
		}
		
		public static event EventHandler<LogEventArgs> Added;
		private static void OnAdded(LogLevel logLevel, string message)
		{
			var added = Added;
			if (added == null) return;
			added(null, new LogEventArgs(logLevel, message));
		}
		
		public static void Add(LogLevel logLevel, string message)
		{
			if (logLevel == LogLevel.None) throw new ArgumentException("LogLevel.None is not valid for add operations");
			if (logLevel > LogLevelFilter) return;
			OnAdded(logLevel, message);
		}
		
		public static void Add(LogLevel logLevel, string format, params object[] args)
		{
			if (logLevel == LogLevel.None) throw new ArgumentException("LogLevel.None is not valid for add operations");
			if (logLevel > LogLevelFilter) return;
			OnAdded(logLevel, string.Format(format, args));
		}
		
		public static void Add(LogLevel logLevel, Func<string> func)
		{
			if (logLevel == LogLevel.None) throw new ArgumentException("LogLevel.None is not valid for add operations");
			if (logLevel > LogLevelFilter) return;
			OnAdded(logLevel, func());			
		}
		
		public static void Add(Exception ex)
		{
			Add(LogLevel.Error, ex.Message);
			Add(LogLevel.Verbose, ex.ToString());
		}
	}
}
