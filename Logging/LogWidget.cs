// 
//  LogWidget.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.ComponentModel;
using System.Collections.Generic;

using GnomeRDP.Logging;

using Gtk;

namespace GnomeRDP
{
	[ToolboxItem(true)]
	public partial class LogWidget : Gtk.Bin
	{
		private Queue<LogEventArgs> logEvents = new Queue<LogEventArgs>();

		private bool destroyed = false;
				
		public LogWidget ()
		{
			this.Build ();
			
			string logLevelName = Log.LogLevelFilter.ToString();
			string[] names = Enum.GetNames(typeof(LogLevel));
			for (int i = 0; i < names.Length; i++) 
			{
				cbLogLevelFilter.AppendText(names[i]);
				if (names[i] == logLevelName) cbLogLevelFilter.Active = i;
			}
			
			Log.Added += (sender, e)  =>
			{
				lock(logEvents)
				{
					logEvents.Enqueue(e);
				}
			};
									
			GLib.Timeout.Add(1000, () => 
			{ 
				Update(); 
				return destroyed == false;
			});
		}
		
		private void Update()
		{
			TextBuffer textBuffer = textview.Buffer;
			
			while (true)
			{
				LogEventArgs e;
				lock (logEvents)
				{
					if (logEvents.Count == 0) return;
					e = logEvents.Dequeue();
				}

				string text = string.Format("{0}: {1}{2}", e.logLevel, e.message, Environment.NewLine);
				
				TextIter endIter = textBuffer.EndIter;
				textBuffer.Insert(ref endIter, text);
			}
		}
		
		protected virtual void OnCbLogLevelFilterChanged (object sender, System.EventArgs e)
		{
			try
			{
				LogLevel logLevel = (LogLevel)Enum.Parse(typeof(LogLevel), cbLogLevelFilter.ActiveText);
				
				Log.LogLevelFilter = logLevel;
			}
			catch
			{
			}
		}
		
		protected override void OnDestroyed ()
		{
			destroyed = true;
			
			base.OnDestroyed ();
		}
	}
}
