gnome-rdp (0.3.0.9-3) unstable; urgency=low

  * [69004af] Automake 1.11.2 enforces C-libs-only in pkglib, so reuse 
    the custom-defined programfilesdir for installing the main assembly.

 -- Jo Shields <directhex@apebox.org>  Fri, 20 Jan 2012 13:18:49 +0000

gnome-rdp (0.3.0.9-2) unstable; urgency=low

  * [e89b64a] add patch to fix vnc connections
    use xtightvncviewer, tight-vncviewer does not exist in debian
    xtightvncviewer does not accepted -user flag and it must receive
    a server to connect to.

 -- Julian Taylor <jtaylor.debian@googlemail.com>  Tue, 11 Oct 2011 21:49:51 +0100

gnome-rdp (0.3.0.9-1) unstable; urgency=low

  * new upstream version

 -- Julian Taylor <jtaylor.debian@googlemail.com>  Tue, 07 Jun 2011 17:18:42 +0200

gnome-rdp (0.2.3-4) unstable; urgency=low

  [ David Paleino ]
  * debian/control: updated my e-mail address

  [ Iain Lane ]
  * debian/control:
   + Bump mono-devel build-dep to >= 2.4.3 and remove all build-depends on
     packages now provided transitively by mono-devel.
   + Replace library build-depends with -dev counterparts which now contain
     the pcfiles. (Closes: #564367)
  * debian/control, debian/rules, debian/patches/*: Add quilt patchsys and
    patch source to refer to gnome-keyring-sharp's now versioned pcfile. 

 -- Iain Lane <laney@ubuntu.com>  Sun, 10 Jan 2010 10:58:19 +0000

gnome-rdp (0.2.3-3) unstable; urgency=low

  * debian/control:
    + No-change bump to Standards 3.8.1
    + Alter build dependencies for Gnome# 2.24 transition (Closes: #527793)
    + Remove strange duplicate entry for Gnome#

 -- Jo Shields <directhex@apebox.org>  Fri, 08 May 2009 00:54:06 +0100

gnome-rdp (0.2.3-2) unstable; urgency=low

  * Upload to unstable.

 -- Mirco Bauer <meebey@debian.org>  Fri, 06 Mar 2009 00:30:53 +0100

gnome-rdp (0.2.3-1) experimental; urgency=low

  [ David Paleino ]
  * New upstream release:
    - upstream developers changed
    - added support for GNOME Keyring
  * Package adopted by pkg-cli-apps team
  * debian/control:
    - Standards-Version updated to 3.8.0 (no changes needed)
    - Homepage updated
    - Build-Depends-Indep updated:
      + libmono-system2.0-cil, libmono-system-data2.0-cil,
        libgnome-keyring1.0-cil added
      + autotools-dev removed
      + nant added
      + libmono1.0-cil and libmono-sqlite1.0-cil bumped to libmono2.0-cil
        and libmono-sqlite2.0-cil
      + mono-mcs removed, in favour of mono-devel
    - Build-Depends updated:
      + debhelper dependency to >= 6 (also debian/compat)
      + removed quilt
    - Dependency fields wrapped (improves diffs readibility)
  * debian/patches/:
    - all removed, integrated upstream.
  * debian/rules:
    - updated to use NAnt as build system
    - removed bits of quilt patching
    - remove spurious bytes at the beginning of /usr/bin/gnome-rdp:
      NAnt bug?
  * debian/copyright updated
  * debian/menu updated
  * debian/gnome-rdp.xpm added

  [ Mirco Bauer ]
  * debian/control:
    + Updated Vcs-* fields to reflect the new location of the source package
      (pkg-cli-apps).
    + Versioned mono-devel build-dep to >= 2.0, as older versions (as found in
      etch) are not shipping the needed applications.
  * debian/rules:
    + Implemented get-orig-source target.

 -- David Paleino <d.paleino@gmail.com>  Sun, 23 Nov 2008 22:56:57 +0100

gnome-rdp (0.2.2-6) unstable; urgency=low

  * Urgency set to medium because of Serious bugs being fixed.
  * debian/patches/:
    - 09-vte-sharp-0.16.patch added (Closes: #470821)
    - 10-glib-sharp-2.12.patch added (Closes: #470340, #470820)
  * debian/control updated:
    - libvte dependency updated
  * debian/copyright updated to machine-readable format

 -- David Paleino <d.paleino@gmail.com>  Thu, 20 Mar 2008 15:24:13 +0100

gnome-rdp (0.2.2-5) unstable; urgency=low

  * debian/patches - converting to quilt patch system
    - 04-add_it.po.patch updated (did not patch configure)
  * debian/control:
    - added libmono2.0-cil to Build-Depends-Indep (Closes: #458667)
    - added quilt to Build-Depends (dpatch removed from B-D-I)
    - Standards-Version bumped to 3.7.3.0
    - added Vcs-Svn and Vcs-Browser fields
    - long description updated
  * debian/rules:
    - updated to support quilt

 -- David Paleino <d.paleino@gmail.com>  Wed, 02 Jan 2008 23:44:56 +0100

gnome-rdp (0.2.2-4) unstable; urgency=low

  * debian/patches/*:
    - 00list - updated to include 08-fix-computer_field_length.dpatch
    - 06-fix_sql_queries.dpatch - removed useless diff sections
    - 08-fix_computer_field_length.dpatch - added; Computer field needs
      to be longer for DNS registered name(FQDN) where user logon
      domain is different (Closes: #444688)
  * debian/control:
    - Build-Depends-Indep fixed to use libmono-sqlite2.0-cil instead of
      libmono-sqlite1.0-cil
    - Added Homepage field instead of the old pseudo-field in the long
      description
  * debian/menu fixed, now the package is in Applications/Network/
    /Communications

 -- David Paleino <d.paleino@gmail.com>  Sun, 30 Sep 2007 15:01:37 +0200

gnome-rdp (0.2.2-3) unstable; urgency=medium

  * debian/control:
    - gnome-rdp is not architecture dependent, CLI Policy violation
      3.1.1 (Closes: #431262)
    - fixing Build-Depends to use real packages, not meta-ones.
  * debian/rules:
    - since it is an architecture-independent package, all the dh_*
      calls have been moved under the binary-indep target.
  * debian/patches/*:
    - 01-fix_vncviewer.dpatch - xtightvncviewer not called correctly
      (Closes: #431717) - xtightvncviewer doesn't support command-line
      switches for username and password.
    - 06-fix_sql_queries.dpatch - Apostroph in session name causes
      configuration error (Closes: #431714) - single quotes escaped
      through Replace() calls
    - 07-fix_gnome-rdp.desktop.dpatch added to remove deprecated
      Encoding field in gnome-rdp.desktop

 -- David Paleino <d.paleino@gmail.com>  Mon, 14 Sep 2007 14:35:24 +0200

gnome-rdp (0.2.2-2) unstable; urgency=low

  * FTBFS: Tries to use ~/.wapi (Closes: #426383)

 -- David Paleino <d.paleino@gmail.com>  Tue, 29 May 2007 09:14:21 +0200

gnome-rdp (0.2.2-1) unstable; urgency=low

  * Initial release (Closes: #425063)

 -- David Paleino <d.paleino@gmail.com>  Fri, 18 May 2007 22:44:57 +0200

