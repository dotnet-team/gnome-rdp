// 
//  RdpProfileDialog.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;

namespace GnomeRDP.Rdp
{
	public partial class RdpProfileDialog : Gtk.Dialog
	{
		public RdpProfileDialog (RdpProfile profile)
		{
			this.Build ();

			txtDescription.Text = profile.Description;
			
			string rdpVersionName = profile.RdpVersion.ToString();
			string[] rdpVersionNames = Enum.GetNames(typeof(RdpVersion));
			for (int i = 0; i < rdpVersionNames.Length; i++) 
			{
				cbRdpVersion.AppendText(rdpVersionNames[i]);
				if (rdpVersionNames[i] == rdpVersionName) cbRdpVersion.Active = i;
			}
			
			txtKeyboardlayout.Text = profile.KeyboardLayout;
			
			cbeWidth.AppendText("640");
			cbeWidth.AppendText("800");
			cbeWidth.AppendText("1024");
			cbeWidth.AppendText("1280");
			cbeWidth.AppendText("1680");
			cbeWidth.Entry.Text = profile.Width.ToString();

			cbeHeight.AppendText("480");
			cbeHeight.AppendText("600");
			cbeHeight.AppendText("768");
			cbeHeight.AppendText("1024");
			cbeHeight.AppendText("1050");
			cbeHeight.Entry.Text = profile.Height.ToString();
			
			string colorDepthName = profile.RdpColorDepth.ToString();
			string[] colorDepthNames = Enum.GetNames(typeof(RdpColorDepth));
			for (int i = 0; i < colorDepthNames.Length; i++) 
			{
				cbColorDepth.AppendText(colorDepthNames[i]);
				if (colorDepthNames[i] == colorDepthName) cbColorDepth.Active = i;
			}
			
			string experienceName = profile.RdpExperience.ToString();
			string[] experienceNames = Enum.GetNames(typeof(RdpExperience));
			for (int i = 0; i < experienceNames.Length; i++) 
			{
				cbExperience.AppendText(experienceNames[i]);
				if (experienceNames[i] == experienceName) cbExperience.Active = i;
			}
			
			string soundName = profile.RdpSound.ToString();
			string[] soundNames = Enum.GetNames(typeof(RdpSound));
			for (int i = 0; i < soundNames.Length; i++)
			{
				cbSound.AppendText(soundNames[i]);
				if (soundNames[i] == soundName) cbSound.Active = i;
			}
			
			chkFullScreen.Active = profile.FullScreen;
			chkEnableCompression.Active = profile.EnableRdpCompression;
			chkAttachToConsole.Active = profile.AttachToConsole;
		}
		
		public string Description
		{
			get
			{
				return txtDescription.Text;
			}
		}
		
		public RdpVersion RdpVersion
		{
			get
			{
				return (RdpVersion)Enum.Parse(typeof(RdpVersion), cbRdpVersion.ActiveText);
			}
		}
		
		public string KeyboardLayout
		{
			get
			{
				return txtKeyboardlayout.Text;
			}
		}
		
		public int RdpWidth
		{
			get
			{
				int width;
				if (int.TryParse(cbeWidth.Entry.Text, out width) == false) return 640;
				return width;
			}
		}
		
		public int RdpHeight
		{
			get
			{
				int height;
				if (int.TryParse(cbeHeight.Entry.Text, out height) == false) return 480;
				return height;
			}
		}
		
		public RdpColorDepth RdpColorDepth
		{
			get
			{
				return (RdpColorDepth)Enum.Parse(typeof(RdpColorDepth), cbColorDepth.ActiveText);
			}
		}
		
		public RdpExperience RdpExperience
		{
			get
			{
				return (RdpExperience)Enum.Parse(typeof(RdpExperience), cbExperience.ActiveText);
			}
		}
		
		public RdpSound RdpSound
		{
			get
			{
				return (RdpSound)Enum.Parse(typeof(RdpSound), cbSound.ActiveText);
			}
		}
		
		public bool FullScreen
		{
			get
			{
				return chkFullScreen.Active;
			}
		}
		
		public bool EnableCompression
		{
			get
			{
				return chkEnableCompression.Active;
			}
		}
		
		public bool AttachToConsole
		{
			get
			{
				return chkAttachToConsole.Active;
			}
		}
	}
}
