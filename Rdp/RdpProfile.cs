//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.Collections.Generic;
using System.Text;

using GnomeRDP.Profiles;
using GnomeRDP.Identies;

namespace GnomeRDP.Rdp
{
	public class RdpProfile : Profile
	{
		public RdpProfile() 
			: base(Protocol.rdp, Program.CreateID(), "")
		{
		}
		
		private RdpProfile(Dictionary<string, string> dictionary)
			: base(Protocol.rdp, dictionary)
		{
			rdpVersion = (RdpVersion)Enum.Parse(typeof(RdpVersion), dictionary[Fields.rdpVersion]);
			keyboardLayout = dictionary[Fields.keyboardLayout];
			width = int.Parse(dictionary[Fields.width]);
			height = int.Parse(dictionary[Fields.height]);
			fullScreen = bool.Parse(dictionary[Fields.fullScreen]);
			windowTitle = dictionary[Fields.windowTitle];
			attachToConsole = bool.Parse(dictionary[Fields.attachToConsole]);
			rdpExperience = (RdpExperience)Enum.Parse(typeof(RdpExperience), dictionary[Fields.rdpExperience]);
			rdpColorDepth = (RdpColorDepth)Enum.Parse(typeof(RdpColorDepth), dictionary[Fields.rdpColorDepth]);
			rdpSound = (RdpSound)Enum.Parse(typeof(RdpSound), dictionary[Fields.rdpSound]);
		}
		
		private new static class Fields
		{
			public const string rdpVersion = "RdpVersion";
			public const string keyboardLayout = "KeyboardLayout";
			public const string width = "Width";
			public const string height = "Height";
			public const string fullScreen = "FullScreen";
			public const string windowTitle = "WindowTitle";
			public const string attachToConsole = "AttachToConsole";
			public const string rdpExperience = "RdpExperience";
			public const string rdpColorDepth = "RdpColorDepth";	
			public const string rdpSound = "RdpSound";
		}
		
		public static RdpProfile Create(Dictionary<string, string> dictionary)
		{
			return new RdpProfile(dictionary);
		}

		public static Dictionary<string, string> ToDictionary(RdpProfile item)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			
			dictionary[Profile.Fields.id] = item.Id;
			dictionary[Profile.Fields.description] = item.Description;

			dictionary[Fields.rdpVersion] = item.RdpVersion.ToString();
			dictionary[Fields.keyboardLayout] = item.KeyboardLayout;
			dictionary[Fields.width] = item.Width.ToString();
			dictionary[Fields.height] = item.Height.ToString();
			dictionary[Fields.fullScreen] = item.FullScreen.ToString();
			dictionary[Fields.windowTitle] = item.WindowTitle;
			dictionary[Fields.attachToConsole] = item.AttachToConsole.ToString();
			dictionary[Fields.rdpExperience] = item.RdpExperience.ToString();
			dictionary[Fields.rdpColorDepth] = item.RdpColorDepth.ToString();
			dictionary[Fields.rdpSound] = item.RdpSound.ToString();
			
			return dictionary;
		}
						
		private RdpVersion rdpVersion = RdpVersion.Version5;
		public RdpVersion RdpVersion
		{
			get
			{
				return rdpVersion;
			}
			set
			{
				rdpVersion = value;
			}
		}
		
		private string keyboardLayout = "en-us";
		public string KeyboardLayout
		{
			get
			{
				return keyboardLayout;
			}
			set
			{
				keyboardLayout = value;
			}
		}
		
		private int width = 800;
		public int Width
		{
			get
			{
				return width;
			}
			set
			{
				if (value <= 0) throw new ArgumentException("Value must be greater than zero.");
				width = value;
			}			
		}
		
		private int height = 600;
		public int Height
		{
			get
			{
				return height;
			}
			set
			{
				if (value <= 0) throw new ArgumentException("Value must be greater than zero.");
				height = value;
			}	
		}
		
		private bool fullScreen;
		public bool FullScreen
		{
			get
			{
				return fullScreen;
			}
			set
			{
				fullScreen = value;
			}
		}
		
		private string windowTitle;
		public string WindowTitle
		{
			get
			{
				return windowTitle;
			}
			set
			{
				windowTitle = value;
			}
		}
		
		private bool attachToConsole;
		public bool AttachToConsole
		{
			get
			{
				return attachToConsole;
			}
			set
			{
				attachToConsole = value;
			}
		}
		
		private RdpColorDepth rdpColorDepth = RdpColorDepth.Bpp16;
		public RdpColorDepth RdpColorDepth	
		{
			get
			{
				return rdpColorDepth;
			}
			set
			{
				rdpColorDepth = value;
			}
		}
		
		private bool enableRdpCompression = true;
		public bool EnableRdpCompression
		{
			get
			{
				return enableRdpCompression;
			}
			set
			{
				enableRdpCompression = value;
			}
		}
			
		private RdpExperience rdpExperience = RdpExperience.Lan;
		public RdpExperience RdpExperience
		{
			get
			{
				return rdpExperience;
			}
			set
			{
				rdpExperience = value;
			}
		}
		
		private RdpSound rdpSound = RdpSound.Local;
		public RdpSound RdpSound
		{
			get
			{
				return rdpSound;
			}
			set
			{
				rdpSound = value;
			}
		}
		
		public override bool IsLike (Profile other)
		{
			var otherRdp = other as RdpProfile;
			if (otherRdp == null) return false;
			
			return AttachToConsole == otherRdp.AttachToConsole
				&& EnableRdpCompression == otherRdp.EnableRdpCompression
				&& FullScreen == otherRdp.FullScreen
				&& Height == otherRdp.Height
				&& Width == otherRdp.Width
				&& KeyboardLayout == otherRdp.KeyboardLayout
				&& RdpColorDepth == otherRdp.RdpColorDepth
				&& RdpExperience == otherRdp.RdpExperience
				&& RdpSound == otherRdp.RdpSound
				&& RdpVersion == otherRdp.RdpVersion
				&& WindowTitle == otherRdp.WindowTitle;
		}

		
		public string ToCommandLineArguements(string server, Identity identity, bool hidePassword)
		{	
			StringBuilder result = new StringBuilder();

			string domain = identity.Domain;
			if (string.IsNullOrEmpty(domain) == false)
			{
				result.AppendFormat(" -d {0}", domain);
			}
			
			string username = identity.Username;
			if (string.IsNullOrEmpty(username) == false) 
			{
				string format = username.Contains(" ") ? " -u \"{0}\"" : " -u {0}";
				result.AppendFormat(format, username);
			}
		
			string password;
			if (hidePassword)
			{
				password = "<password>";
			}
			else
			{
				password = identity.GetPassword(server, Protocol.rdp);
			}
					
			if (string.IsNullOrEmpty(password) == false)
			{
				//string format = password.Contains(" ") ? " -p \"{0}\"" : " -p {0}";
				string format = " -p \"{0}\"";
				result.AppendFormat(format, password);
			}
			
			if (string.IsNullOrEmpty(keyboardLayout) == false)
			{
				result.AppendFormat(" -k {0}", keyboardLayout);
			}
			
			result.AppendFormat(" -g {0}x{1}", width, height);
			
			if (fullScreen)
			{
				result.Append(" -f");
			}
			
			if (string.IsNullOrEmpty(windowTitle) == false)
			{
				result.AppendFormat(" -T \"{0}\"", windowTitle);				
			}
			
			result.AppendFormat(" -a {0}", (byte)rdpColorDepth);
		
			if (enableRdpCompression)
			{
				result.Append(" -z");
			}
			
			result.AppendFormat(" -x {0}", (char)rdpExperience);
			
			if (attachToConsole)
			{
				result.Append(" -0");
			}
			
			switch (rdpVersion)
			{
				case RdpVersion.Version4:
					result.Append(" -4");
					break;
					
				case RdpVersion.Version5:
				default:
					result.Append(" -5");
					break;
			}
			
			switch (rdpSound)
			{
				case RdpSound.Local:
					result.Append(" -r sound:local");
				break;
					
				case RdpSound.Remote:
					result.Append(" -r sound:remote");
				break;
					
				case RdpSound.Off:
					result.Append(" -r sound:off");
				break;
			}
			
			result.Append(string.Format(" {0}", server));
			
			return result.ToString();
		}
	}
}
