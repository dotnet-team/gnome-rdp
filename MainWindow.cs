//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Gtk;

using GnomeRDP.Identies;
using GnomeRDP.Sessions;
using GnomeRDP.Logging;
using GnomeRDP.Profiles;
using GnomeRDP.Rdp;
using GnomeRDP.Ssh;
using GnomeRDP.Vnc;

namespace GnomeRDP
{
	public partial class MainWindow: Gtk.Window
	{			
		private StatusIcon statusIcon;
		private Gtk.Action actionQuit;
				
		private const string sessionKey = "Session";
		
		public MainWindow(): base (Gtk.WindowType.Toplevel)
		{
			Build ();
			
			this.DeleteEvent += (s, e) =>
			{
				Visible = false;
				e.RetVal = true;
			};
			
			this.actionQuit = new Gtk.Action("QuitAction", "Quit");
			this.actionQuit.Activated+= (s, e) => Application.Quit();
			
			this.statusIcon = new StatusIcon(ResourceLoader.Find(ResourceLoader.Icons.gnomeRdp));
			this.statusIcon.Visible = true;
			this.statusIcon.Tooltip = "GnomeRDP";
			this.statusIcon.Activate += OnStatusIcon_Activate;
			this.statusIcon.PopupMenu += OnStatusIcon_PopupMenu;	

			this.Icon = ResourceLoader.Find(ResourceLoader.Icons.gnomeRdp);
		}
		
		private void OnStatusIcon_Activate(object sender, EventArgs e)
		{
			Visible = !Visible;
		}
		
		private void OnStatusIcon_PopupMenu(object sender, PopupMenuArgs e)
		{
			try
			{
				Menu topMenu = new Menu();
				topMenu.Popup();
				
				foreach (var group in Program.SessionCollection.Groups) 
				{	
					MenuItem groupMenu = new MenuItem(group);
					topMenu.Append(groupMenu);
					
					Menu subMenu = new Menu();
					foreach(var session in Program.SessionCollection.Items.Where(s => s.Group == group).OrderBy(s => s.Server))
					{
						MenuItem menuItem = new MenuItem(session.MenuFormat);
						menuItem.TooltipText = session.Tooltip;
						menuItem.Activated += PopupMenuItem_Activated;
						menuItem.Data[sessionKey] = session;
											
						subMenu.Append(menuItem);
					}
					groupMenu.Submenu = subMenu;
				}

				topMenu.Append(new SeparatorMenuItem());

				foreach (var session in Program.SessionCollection.Items.Where(s => string.IsNullOrEmpty(s.Group)).OrderBy(s => s.Server)) 
				{
					MenuItem menuItem = new MenuItem(session.MenuFormat);
					menuItem.TooltipText = session.Tooltip;
					menuItem.Activated += PopupMenuItem_Activated;
					menuItem.Data[sessionKey] = session;					
					
					topMenu.Append(menuItem);
				}
				
				topMenu.Append(new SeparatorMenuItem());
				topMenu.Append(actionQuit.CreateMenuItem());
				topMenu.ShowAll();
//				topMenu.Popup();
			}
			catch
			{
			}
		}	
		
		private void PopupMenuItem_Activated(object sender, EventArgs e)
		{
			try
			{
				MenuItem menuItem = (MenuItem) sender;				
				Session session = (Session)menuItem.Data[sessionKey];
				Program.SessionCollection.Connect(session);
			}
			catch
			{
			}
		}
		
		protected virtual void OnNewRdpActionActivated (object sender, System.EventArgs e)
		{
		}
		
		protected virtual void OnNewVncActionActivated (object sender, System.EventArgs e)
		{
		}
		
		protected virtual void OnNewSshActionActivated (object sender, System.EventArgs e)
		{
		}
		
		
		
		
	}
}