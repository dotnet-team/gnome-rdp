

# Warning: This is an automatically generated file, do not edit!

if ENABLE_DEBUG
ASSEMBLY_COMPILER_COMMAND = gmcs
ASSEMBLY_COMPILER_FLAGS =  -noconfig -codepage:utf8 -warn:3 -optimize- -debug -define:DEBUG "-define:DEBUG, TRACE" "-main:GnomeRDP.Program"
ASSEMBLY = bin/Debug/gnome-rdp.exe
ASSEMBLY_MDB = $(ASSEMBLY).mdb
COMPILE_TARGET = exe
PROJECT_REFERENCES = 
BUILD_DIR = bin/Debug

GNOME_RDP_EXE_MDB_SOURCE=bin/Debug/gnome-rdp.exe.mdb
GNOME_RDP_EXE_MDB=$(BUILD_DIR)/gnome-rdp.exe.mdb

endif

if ENABLE_RELEASE
ASSEMBLY_COMPILER_COMMAND = gmcs
ASSEMBLY_COMPILER_FLAGS =  -noconfig -codepage:utf8 -warn:4 -optimize- "-main:GnomeRDP.Program"
ASSEMBLY = bin/Release/gnome-rdp.exe
ASSEMBLY_MDB = 
COMPILE_TARGET = exe
PROJECT_REFERENCES = 
BUILD_DIR = bin/Release

GNOME_RDP_EXE_MDB=

endif

AL=al2
SATELLITE_ASSEMBLY_NAME=$(notdir $(basename $(ASSEMBLY))).resources.dll

PROGRAMFILES = \
	$(GNOME_RDP_EXE_MDB)  

BINARIES = \
	$(GNOME_RDP)  


RESGEN=resgen2
	
all: $(ASSEMBLY) $(PROGRAMFILES) $(BINARIES) 

FILES = \
	gtk-gui/generated.cs \
	MainWindow.cs \
	Program.cs \
	AssemblyInfo.cs \
	Identities/IdentityDialog.cs \
	gtk-gui/GnomeRDP.MainWindow.cs \
	Identities/Identity.cs \
	Identities/IdentityCollection.cs \
	Database/Sqlite.cs \
	ApplicationDataFiles/FileManager.cs \
	gtk-gui/GnomeRDP.Identies.IdentityDialog.cs \
	Logging/Log.cs \
	KeyringProxy.cs \
	Protocol.cs \
	Rdp/RdpProfile.cs \
	Rdp/RdpVersion.cs \
	Rdp/RdpSound.cs \
	Rdp/RdpExperience.cs \
	Rdp/RdpColorDepth.cs \
	Logging/LogEventArgs.cs \
	Logging/LogLevel.cs \
	Profiles/Profile.cs \
	Rdp/RdpProfileDialog.cs \
	Ssh/SshProfile.cs \
	Vnc/VncProfile.cs \
	Sessions/Session.cs \
	Sessions/SessionCollection.cs \
	Collection.cs \
	gtk-gui/GnomeRDP.Rdp.RdpProfileDialog.cs \
	Sessions/SessionDialog.cs \
	gtk-gui/GnomeRDP.Sessions.SessionDialog.cs \
	Sessions/SessionsWidget.cs \
	gtk-gui/GnomeRDP.Sessions.SessionsWidget.cs \
	Identities/IdentitiesWidget.cs \
	gtk-gui/GnomeRDP.Identies.IdentitiesWidget.cs \
	Profiles/ProfilesWidget.cs \
	gtk-gui/GnomeRDP.Profiles.ProfilesWidget.cs \
	ResourceLoader.cs \
	Sessions/SessionHost.cs \
	Identities/PasswordDialog.cs \
	gtk-gui/GnomeRDP.PasswordDialog.cs \
	Logging/LogWidget.cs \
	gtk-gui/GnomeRDP.LogWidget.cs \
	Vnc/VncEncoding.cs \
	Vnc/VncColorDepth.cs \
	Vnc/VncProfileDialog.cs \
	gtk-gui/GnomeRDP.Vnc.VncProfileDialog.cs \
	Profiles/ProfileCollection.cs \
	Ssh/SshProfileDialog.cs \
	gtk-gui/GnomeRDP.SshProfileDialog.cs \
	Interfaces.cs 

DATA_FILES = 

RESOURCES = \
	gtk-gui/gui.stetic \
	Resources/gnome-rdp-icon.png,GnomeRDP.Resources.gnome-rdp-icon.png \
	Resources/group_16.png,GnomeRDP.Resources.group_16.png \
	Resources/rdp.png,GnomeRDP.Resources.rdp.png \
	Resources/rdp_16.png,GnomeRDP.Resources.rdp_16.png \
	Resources/ssh.png,GnomeRDP.Resources.ssh.png \
	Resources/ssh_16.png,GnomeRDP.Resources.ssh_16.png \
	Resources/vnc.png,GnomeRDP.Resources.vnc.png \
	Resources/vnc_16.png,GnomeRDP.Resources.vnc_16.png 

EXTRAS = \
	app.desktop \
	Identities \
	Sessions \
	Database \
	ApplicationDataFiles \
	Logging \
	Rdp \
	Vnc \
	Ssh \
	Resources \
	ChangeLog \
	gnome-rdp.in 

REFERENCES =  \
	System \
	Mono.Posix \
	$(GTK_SHARP_20_LIBS) \
	$(GLIB_SHARP_20_LIBS) \
	$(GLADE_SHARP_20_LIBS) \
	System.Data \
	System.Core \
	$(GNOME_KEYRING_SHARP_10_LIBS) \
	Mono.Data.Sqlite

DLL_REFERENCES = 

CLEANFILES = $(PROGRAMFILES) $(BINARIES) 

include $(top_srcdir)/Makefile.include

GNOME_RDP = $(BUILD_DIR)/gnome-rdp

$(eval $(call emit-deploy-wrapper,GNOME_RDP,gnome-rdp,x))


$(eval $(call emit_resgen_targets))
$(build_xamlg_list): %.xaml.g.cs: %.xaml
	xamlg '$<'

$(ASSEMBLY_MDB): $(ASSEMBLY)

$(ASSEMBLY): $(build_sources) $(build_resources) $(build_datafiles) $(DLL_REFERENCES) $(PROJECT_REFERENCES) $(build_xamlg_list) $(build_satellite_assembly_list)
	mkdir -p $(shell dirname $(ASSEMBLY))
	$(ASSEMBLY_COMPILER_COMMAND) $(ASSEMBLY_COMPILER_FLAGS) -out:$(ASSEMBLY) -target:$(COMPILE_TARGET) $(build_sources_embed) $(build_resources_embed) $(build_references_ref)
