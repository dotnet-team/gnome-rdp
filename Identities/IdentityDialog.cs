//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;

using Gtk;

namespace GnomeRDP.Identies
{
	public partial class IdentityDialog : Gtk.Dialog
	{	
		public IdentityDialog()
		{
			this.Build();
		}
		
		public IdentityDialog(Identity identity) : this()
		{			
			if (identity == null) return;
			
			entryDescription.Text = identity.Description;
			entryDomain.Text = identity.Domain;
			entryUsername.Text = identity.Username;
			chkSavePassword.Active = identity.SavePassword;
		}

		protected virtual void OnButtonOkClicked (object sender, System.EventArgs e)
		{
			this.Respond(Gtk.ResponseType.Ok);
		}	
				
		protected virtual void OnButtonClearSavedPasswordsClicked (object sender, System.EventArgs e)
		{
			MessageDialog dlg = new MessageDialog(null, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, "This will clear all saved passwords for this identity. Are you sure?");
			try
			{
				if(dlg.Run() != (int)ResponseType.Yes) return;
			
				KeyringProxy.ClearSavedPasswords(Username, Domain);
			}
			finally
			{
				dlg.Destroy();
			}
		}
		
		protected virtual void OnButtonReplaceSavedPasswordsClicked (object sender, System.EventArgs e)
		{
			string username = Username;
			string domain = Domain;
			
			PasswordDialog dlg = new PasswordDialog("All", "All", domain, username);
			try
			{
				if (dlg.Run() != (int)ResponseType.Ok) return;
				
				KeyringProxy.ReplaceSavedPasswords(username, domain, dlg.Password);
			}
			finally
			{
				dlg.Destroy();
			}
		}
		
		public string Domain
		{
			get
			{
				return entryDomain.Text;
			}
		}
				
		public string Username
		{
			get
			{
				return entryUsername.Text;
			}
		}
		
		public bool SavePassword
		{
			get
			{
				return chkSavePassword.Active;
			}
		}
		
		public string Description
		{
			get
			{
				return entryDescription.Text;
			}
		}
	}
}
