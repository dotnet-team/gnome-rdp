// 
//  IdentitiesWidget.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Collections.Generic;
using System.ComponentModel;	

using Gtk;	

using GnomeRDP.Identies;
using GnomeRDP.Sessions;
using GnomeRDP.Logging;
using GnomeRDP.Profiles;
using GnomeRDP.Rdp;
using GnomeRDP.Ssh;
using GnomeRDP.Vnc;

namespace GnomeRDP.Identies
{
	[ToolboxItem(true)]
	public partial class IdentitiesWidget : Gtk.Bin
	{
		private Menu menu = new Menu();
				
		public IdentitiesWidget ()
		{
			this.Build ();
			
			Initialize(); 
		}

		private static class Columns
		{
			public const int id = 0;
			public const int domain = 1;
			public const int username = 2;
			public const int description = 3;
		}
		
		private void Initialize()
		{
			treeView.AppendColumn(new TreeViewColumn("Domain", new CellRendererText(), "text", Columns.domain));
			treeView.AppendColumn(new TreeViewColumn("Username", new CellRendererText(), "text", Columns.username));
			treeView.AppendColumn(new TreeViewColumn("Description", new CellRendererText(), "text", Columns.description));
			treeView.Selection.Mode = SelectionMode.Multiple;
			treeView.RulesHint = true;
			
			treeView.Model = new ListStore(typeof(string), typeof(string), typeof(string), typeof(string));
		
			Resync();

			// Initialize Context Menu
			var propertiesMenuItem = new MenuItem("Properties");
			propertiesMenuItem.Activated += OnMenuPropertiesActivated;
			menu.Add(propertiesMenuItem);

			var deleteMenuItem = new MenuItem("Delete");
			deleteMenuItem.Activated += OnMenuDeleteActivated;			
			menu.Add(deleteMenuItem);		
		}
		
		protected virtual void OnNewIdentityActionActivated (object sender, System.EventArgs e)
		{
			IdentityDialog dlg = new IdentityDialog();
			try
			{
				int result = dlg.Run();
				if (result != (int)Gtk.ResponseType.Ok) return;
				
				Identity item = new Identity(Program.CreateID(), dlg.Domain, dlg.Username, dlg.SavePassword, dlg.Description);
				Program.IdentityCollection.Add(item);
			}
			catch(Exception ex)
			{
				Log.Add(ex);
			}
			finally
			{
				dlg.Destroy();
			}

			Resync();
		}
		
		private void Resync()
		{
			try
			{
				ListStore listStore = (ListStore)treeView.Model;			
				listStore.Clear();
				
				foreach (var item in Program.IdentityCollection.ToArray()) 
				{
					listStore.AppendValues(item.Id, item.Domain, item.Username, item.Description);
				}
			}
			catch
			{
			}
		}
	
		private Identity[] GetSelectedIdentities()
		{
			List<Identity> items = new List<Identity>();
			try
			{
				foreach (var path in treeView.Selection.GetSelectedRows()) 
				{
					TreeIter iter;
					treeView.Model.GetIter(out iter, path);
								
					string id = (string)treeView.Model.GetValue(iter, Columns.id);
					
					items.Add(Program.IdentityCollection.Find(id));
				}
				return items.ToArray();
			}
			catch
			{
				return new Identity[0];
			}
		}

		[GLib.ConnectBefore()]
		protected virtual void OnTreeViewButtonPressEvent (object o, Gtk.ButtonPressEventArgs args)
		{
			try
			{	
				if (args.Event.Button != 3) 
				{
					args.RetVal = false;
					return;
				}
				
				TreePath path;
				if (treeView.GetPathAtPos((int)args.Event.X, (int)args.Event.Y, out path) == false) return;
				
				treeView.GrabFocus();
				treeView.SetCursor(path, treeView.Columns[0], false);
				
				
				menu.ShowAll();
				menu.Popup();
				
				args.RetVal = true;
			}
			catch
			{
			}
		}
	
		protected virtual void OnMenuPropertiesActivated (object sender, System.EventArgs e)
		{
			try
			{
				foreach (var identity in GetSelectedIdentities()) 
				{
					IdentityDialog dlg = new IdentityDialog(identity);
					try
					{
						if (dlg.Run() == (int)ResponseType.Ok)
						{
							identity.Set(dlg.Domain, dlg.Username, dlg.SavePassword, dlg.Description);
							Program.IdentityCollection.Save();
						}
					}
					catch(Exception ex)
					{
						Log.Add(ex);
					}
					finally
					{
						dlg.Destroy();
					}
				}
			}
			catch(Exception ex)
			{
				Log.Add(ex);
			}

			Resync();
		
		}

		protected virtual void OnMenuDeleteActivated (object sender, System.EventArgs e)
		{
			try
			{
				Identity[] identities = GetSelectedIdentities();
				if(identities.Length == 0) return;
				
				MessageDialog dlg = new MessageDialog(null, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, "Are you sure you want to delete these {0} item(s)?", identities.Length);
				try
				{
					if(dlg.Run() != (int)ResponseType.Yes) return;
				
					foreach (var identity in identities) 
					{
						Program.IdentityCollection.Remove(identity);
					}
				}
				finally
				{
					dlg.Destroy();
				}
			}
			catch(Exception ex)
			{
				Log.Add(ex);
			}
			
			Resync();
		}		
	}
}
