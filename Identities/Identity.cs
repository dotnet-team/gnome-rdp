//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.Collections.Generic;

using GnomeRDP.Logging;

namespace GnomeRDP.Identies
{
	public class Identity : IIdType, IEquatable<Identity>, ISimiliar<Identity>
	{
		public Identity() : this(Program.CreateID(), "", "", true, "")
		{
		}
		
		public Identity(string id, string domain, string username, bool savePassword, string description)
		{
			this.id = id;
			this.domain = domain;
			this.username = username;
			this.savePassword = savePassword;
			this.description = description;				
		}
		
		private static class Fields
		{
			public const string id = "Id";
			public const string domain = "Domain";
			public const string username = "Username";
			public const string savePassword = "SavePassword";
			public const string description = "Description";
		}
		
		public static Identity Create(Dictionary<string, string> dictionary)
		{
			string id = dictionary[Fields.id];
			string domain = dictionary[Fields.domain];
			string username = dictionary[Fields.username];
			bool savePassword = bool.Parse(dictionary[Fields.savePassword]); 
			
			string description = dictionary.ContainsKey(Fields.description) ? dictionary[Fields.description] : string.Empty;
			
			return new Identity(id, domain, username, savePassword, description);	
		}

		public static Dictionary<string, string> ToDictionary(Identity item)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			
			dictionary[Fields.id] = item.Id;
			dictionary[Fields.domain] = item.Domain;
			dictionary[Fields.username] = item.Username;
			dictionary[Fields.savePassword] = item.SavePassword.ToString();
			dictionary[Fields.description] = item.Description;
			
			return dictionary;
		}
		
		public void Set(string domain, string username, bool savePassword, string description)
		{
			this.domain = domain;
			this.username = username;
			this.savePassword = savePassword;
			this.description = description;
		}
		
		private readonly string id;
		public string Id
		{
			get
			{
				return id;
			}
		}
				
		private string domain;
		public string Domain
		{
			get
			{
				return domain;
			}
			set
			{
				domain = value;
			}
		}
				
		private string username;
		public string Username
		{
			get
			{
				return username;
			}
			set
			{
				username = value;
			}
		}
		
		private bool savePassword;
		public bool SavePassword
		{
			get
			{
				return savePassword;
			}
			set
			{
				savePassword = value;
			}
		}
		
		private string description;
		public string Description
		{
			get
			{
				return description;
			}
			set
			{
				description = value;
			}
		}
		
		public bool Equals (Identity other)
		{
			return id == other.id;
		}
		
		public bool IsLike (Identity other)
		{
			return Domain == other.Domain
				&& Username == other.Username
				&& SavePassword == other.SavePassword;
		}
		
		public override int GetHashCode ()
		{
			return domain.GetHashCode() ^ username.GetHashCode() ^ savePassword.GetHashCode();
		}

		public override string ToString ()
		{
			if (string.IsNullOrEmpty(description) == false) 
			{
				return string.Format(@"{0}\{1} ({2})", domain, username, description);
			}
			else
			{
				return string.Format(@"{0}\{1}", domain, username);
			}		
		}
		
		public string GetPassword(string server, Protocol protocol)
		{
			string domain = Domain;
			string username = Username;
			
			string password = KeyringProxy.GetPassword(username, domain, server, protocol);
			
			if (password == null)
			{
				Program.Invoke(() =>
				{
					PasswordDialog dlg = new PasswordDialog(protocol.ToString(), server, domain, username);
					try
					{
						if (dlg.Run() == (int)Gtk.ResponseType.Ok)
						{
							password = dlg.Password;
						}
					}
					catch (Exception ex)
					{
						Log.Add(ex);
					}
					finally
					{
						dlg.Destroy();
					}				
				}, TimeSpan.FromSeconds(120));
									
				if (password != null && SavePassword)
				{
					KeyringProxy.SetPassword(username, domain, server, protocol, password);
				}	
			}
			
			return password;
		}
	}
}
