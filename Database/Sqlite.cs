//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
//  Contributors
//  Stephen Phillips

using System;
using System.Collections.Generic;
using System.Data;

using Mono.Data.Sqlite;

using GnomeRDP.Identies;
using GnomeRDP.Sessions;
using GnomeRDP.Profiles;
using GnomeRDP.Rdp;
using GnomeRDP.Logging;

namespace GnomeRDP.Database
{
	public static class Sqlite
	{	
		private static string filename = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) + "/.gnome-rdp.db";

		public static void SetDatabasePath(string newPath)
		{
			filename = newPath;
		}
					
		private static SqliteConnection Connect(string filename)
		{			
			string connectString = "URI=file:" + filename + ",version=3,encoding=UTF-8";
			
			SqliteConnection connection = new SqliteConnection(connectString);
			connection.Open();
			
			return connection;
		}
			
		private static class SessionFields
		{
			public const string id = "id";
			public const string parentId = "parentid";
			public const string isCategory = "iscategory";
			public const string sessionName = "sessionname";
			public const string protocol = "protocol";
			public const string computer = "computer";
			public const string user = "user";
			public const string password = "password";
			public const string domain = "domain";
			public const string serverType = "srvtype";
			public const string colorDepth = "colordepth";
			public const string screenResolutionX = "screenresolutionx";
			public const string screenResolutionY = "screenresolutiony";
			public const string soundRedirection = "soundredirection";
			public const string keyboardLanguage = "keyboardlang";
			public const string connectionType = "connectiontype";
			public const string windowMode = "windowmode";
			public const string terminalSize = "terminalsize";
			public const string compressionLevel = "compressionlevel";
			public const string imageQuality = "imagequality";
		}
		
		private static RdpProfile ParseRdpProfile(IDataReader reader)
		{
			int protocol = reader.GetInt32(reader.GetOrdinal(SessionFields.protocol));
			if (protocol != 0) throw new ArgumentException("protocol must be 0 for Rdp");
			
			int serverType = reader.GetInt32(reader.GetOrdinal(SessionFields.serverType));
			int colorDepth = reader.GetInt32(reader.GetOrdinal(SessionFields.colorDepth));
			int width = reader.GetInt32(reader.GetOrdinal(SessionFields.screenResolutionX));
			int height = reader.GetInt32(reader.GetOrdinal(SessionFields.screenResolutionY));
			int soundRedirection = reader.GetInt32(reader.GetOrdinal(SessionFields.soundRedirection));
			string keyboardLanguage = reader.GetString(reader.GetOrdinal(SessionFields.keyboardLanguage));
			int windowMode = reader.GetInt32(reader.GetOrdinal(SessionFields.windowMode));
									
			RdpProfile profile = new RdpProfile();
			profile.RdpVersion = (serverType == 0) ? RdpVersion.Version4 : RdpVersion.Version5;
			profile.RdpColorDepth = IntToRdpColorDepth(colorDepth);
			profile.Width = width;
			profile.Height = height;
			profile.RdpSound = IntToRdpSound(soundRedirection);
			if (profile.RdpSound == RdpSound.Remote)
			{
				profile.AttachToConsole = true;
			}
			profile.KeyboardLayout = keyboardLanguage;
			profile.FullScreen = windowMode > 0 ? true : false;			
			
			return profile;
		}
		
		private static Identity ParseIdentity(IDataReader reader)
		{
			string domain = reader.GetString(reader.GetOrdinal(SessionFields.domain));
			string username = reader.GetString(reader.GetOrdinal(SessionFields.user));
			
			Identity identity = new Identity(Program.CreateID(), domain, username, true, "");

			return identity;
		}
		
		private static RdpColorDepth IntToRdpColorDepth(int colorDepth)
		{
			switch (colorDepth) 
			{
				case 0: return RdpColorDepth.Bpp8;
				case 1: return RdpColorDepth.Bpp15;
				default:
				case 2: return RdpColorDepth.Bpp16;
				case 3: return RdpColorDepth.Bpp24;
				case 4: return RdpColorDepth.Bpp32;
			}	
		}
		
		private static RdpSound IntToRdpSound(int soundRedirection)
		{
			switch (soundRedirection)
			{
				case 0: return RdpSound.Off;
				case 1: return RdpSound.Remote;
				case 2: return RdpSound.Local;
				default: return RdpSound.Unspecified;
			}
		}
								
		public static void ImportSessions()
		{
			try
			{
				using(SqliteConnection connection = Connect(filename))
				{	
					var groups = LoadGroups(connection);
					
					IDbCommand command = connection.CreateCommand();
					// TODO remove where clause after adding Ssh and Vnc support
					command.CommandText = "SELECT * FROM session where protocol=0";
					
					IDataReader reader = command.ExecuteReader();
					while (reader.NextResult())
					{
						string server = reader.GetString(reader.GetOrdinal(SessionFields.computer));
						
						int groupId = reader.GetInt32(reader.GetOrdinal(SessionFields.parentId));
						
						string group;
						if (groups.TryGetValue(groupId, out group) == false) group = null; // Let's be explicit about this behavior.
						
						// HACK workaround for mono 2.4.x compiler bug. remove after moving all deployments to mono 2.6 or higher.
						Identity identity = (Identity)Program.IdentityCollection.FindSimiliarOrAdd(ParseIdentity(reader));
						RdpProfile rdpProfile = (RdpProfile)Program.ProfileCollection.FindSimiliarOrAdd(ParseRdpProfile(reader));
												
						Program.SessionCollection.Add(new Session(server, identity.Id, rdpProfile.Id, group));
					}
				}
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
		}
		
		private static Dictionary<int, string> LoadGroups(SqliteConnection connection)
		{
			var groups = new Dictionary<int, string>();
			
			try
			{
				IDbCommand command = connection.CreateCommand();
				command.CommandText = "SELECT * FROM session where protocol=99";
				
				IDataReader reader = command.ExecuteReader();
				while (reader.NextResult())
				{
					int groupId = reader.GetInt32(reader.GetOrdinal(SessionFields.id));
					string groupName = reader.GetString(reader.GetOrdinal(SessionFields.sessionName));
					groups.Add(groupId, groupName);
				}
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
			
			return groups;
		} 						
	}
}
