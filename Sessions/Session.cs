//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.Collections.Generic;

using GnomeRDP.Identies;
using GnomeRDP.Profiles;
using GnomeRDP.Rdp;

namespace GnomeRDP.Sessions
{
	public class Session : IIdType, IEquatable<Session>
	{
		public const string defaultGroupName = "Default"; 
		
		public Session () : this(Program.CreateID(), null, null, null, null)
		{
		}
				
		public Session(string server, string identityId, string profileId, string group)
			: this(Program.CreateID(), server, identityId, profileId, group)
		{
		}

		private Session(string id, string server, string identityId, string profileId, string group)
		{
			this.id = id;
			this.server = server;
			this.identityId = identityId;
			this.profileId = profileId;
			this.group = group;
		}

		private static class Fields
		{
			public const string id = "Id";
			public const string server = "Server";
			public const string identityId = "IdentityId";
			public const string profileId = "ProfileId";
			public const string group = "Group";
		}
		
		public static Session Create(Dictionary<string, string> dictionary)
		{
			string id = dictionary[Fields.id];
			string server = dictionary[Fields.server];
			string identityId = dictionary[Fields.identityId];
			string profileId = dictionary[Fields.profileId];
			string group = dictionary[Fields.group];
			
			return new Session(id, server, identityId, profileId, group);
		}

		public static Dictionary<string, string> ToDictionary(Session item)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();

			dictionary[Fields.id] = item.Id;
			dictionary[Fields.server] = item.Server;
			dictionary[Fields.identityId] = item.identityId;
			dictionary[Fields.profileId] = item.profileId;
			dictionary[Fields.group] = item.Group;
			
			return dictionary;
		}

		public bool Equals(Session other)
		{
			return id == other.id;
		}

		public override int GetHashCode ()
		{
			return id.GetHashCode();
		}

		private readonly string id;
		public string Id
		{
			get
			{
				return id;
			}
		}
		
		private string server;
		public string Server
		{
			get
			{
				return server;
			}
			set
			{
				server = value;
			}
		}
			
		private string identityId = null;
		public Identity Identity
		{
			get 
			{
				return Program.IdentityCollection.Find(identityId);
			}
			set
			{
				identityId = value.Id;
			}
		}
		
		private string profileId;
		public Profile Profile
		{
			get
			{
				return Program.ProfileCollection.Find(profileId);
			}
			set
			{
				profileId = value.Id;
			}
		}
		
		private string group;
		public string Group
		{
			get
			{
				return group;
			}
			set
			{
				group = value;
			}
		}
		
		public string MenuFormat
		{
			get
			{
				Identity identity = Identity;
				string identityDescription = identity == null ? null : identity.Description;
				string identitySeperator = string.IsNullOrEmpty(identityDescription) ? null : " - ";
				
				Profile profile = Profile;
				string profileDescription = profile == null ? null : profile.Description;
				string profileSeperator = string.IsNullOrEmpty(profileDescription) ? null : " - ";
								
				return string.Format("{0}{1}{2}{3}{4}", server, identitySeperator, identityDescription, profileSeperator, profileDescription);
			}
		}
		
		public string Tooltip
		{
			get
			{
				Identity identity = Identity;
				string domainUser = identity == null ? null : string.Format(@"{0}\{1}", identity.Domain, identity.Username);
				
				Profile profile = Profile;
				string protocol = profile == null ? null : profile.Protocol.ToString();
				string description = profile == null ? null : profile.Description;
								
				return string.Format("{0}@{1}://{2} {3}", domainUser, protocol, server, description);
			}
		}
	}
}
