// 
//  SessionHost.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Collections.Generic;
using System.Threading;
using System.Diagnostics;
using System.IO;

using Mono.Unix;

using GnomeRDP.Logging;

namespace GnomeRDP
{
	public class SessionHost
	{
		private readonly Process process;
		
		public SessionHost (string file, string args, string safeArgs)
		{
			Log.Add(LogLevel.Information, string.Format("Creating process for {0} {1}", file, safeArgs));
			
			process = new Process();
			process.StartInfo.UseShellExecute = false;
			process.StartInfo.RedirectStandardInput = true;
			process.StartInfo.RedirectStandardOutput = true;
			process.StartInfo.RedirectStandardError = true;
			process.StartInfo.CreateNoWindow = false;
			process.StartInfo.FileName = file;
			process.StartInfo.Arguments = args;
			process.StartInfo.WorkingDirectory = "/";
			
			new Thread(new ThreadStart(WorkerThread)).Start();
		}
		
		private void WorkerThread()
		{
			try
			{
				process.EnableRaisingEvents = true;
				process.OutputDataReceived += (s, e) =>
				{
					string data = e.Data;
					if (string.IsNullOrEmpty(data)) return;
					Log.Add(LogLevel.Information, data);
				};
				
				process.ErrorDataReceived += (s, e) => 
				{
					string data = e.Data;
					if (string.IsNullOrEmpty(data)) return;
					Log.Add(LogLevel.Error, data);
				};
				
				process.WaitForInputIdle();
				if (process.Start() == false) throw new Exception("Process failed to start");

				process.BeginOutputReadLine();
				process.BeginErrorReadLine();
				
				while (process.WaitForExit(1000) == false)
				{
					if (Program.IsClosed) 
					{
						process.CloseMainWindow();
						break;
					}
				}

				process.Close();
								
				Log.Add( LogLevel.Information, "Process completed");
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
			finally
			{
				process.Dispose();
			}
		}
		
	}

	public class SessionHostConsole
	{
		private readonly Process process;
		
		private const string terminalProgram = "gnome-terminal";
		
		public SessionHostConsole (string file, string args, string safeArgs, bool fullScreen)
		{
			Log.Add(LogLevel.Information, string.Format("Creating process for {0} {1}", file, safeArgs));
			
			string prefix = (fullScreen) ? "--full-screen" : "";
			string terminalArgs = string.Format("{0} --command=\"{1} {2}\"", prefix, file, args);
			
			process = new Process();
			process.StartInfo.UseShellExecute = false;
			process.StartInfo.RedirectStandardInput = false;
			process.StartInfo.RedirectStandardOutput = false;
			process.StartInfo.RedirectStandardError = false;
			process.StartInfo.CreateNoWindow = true;
			process.StartInfo.FileName = terminalProgram;
			process.StartInfo.Arguments = terminalArgs;
			process.StartInfo.WorkingDirectory = "/";
			
			new Thread(new ThreadStart(WorkerThread)).Start();
		}
		
		private void WorkerThread()
		{
			try
			{				
				process.WaitForInputIdle();
				if (process.Start() == false) throw new Exception("Process failed to start");

				while (process.WaitForExit(1000) == false)
				{
					if (Program.IsClosed) 
					{
						process.CloseMainWindow();
						break;
					}
				}

				process.Close();
								
				Log.Add( LogLevel.Information, "Process completed");
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
			finally
			{
				process.Dispose();
			}
		}
		
	}

}
