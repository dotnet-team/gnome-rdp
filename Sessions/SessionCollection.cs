// 
//  SessionCollection.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Collections.Generic;

using GnomeRDP.ApplicationDataFiles;
using GnomeRDP.Database;
using GnomeRDP.Rdp;
using GnomeRDP.Identies;
using GnomeRDP.Profiles;

namespace GnomeRDP.Sessions
{
	public class SessionCollection : Collection<Session>
	{
		protected override Session[] LoadItems()
		{
			return FileManager.LoadSessions();
		}
						
		public override void Save()
		{
			FileManager.SaveSessions(ToArray());
		}
		
		public IEnumerable<string> Groups
		{
			get
			{
				HashSet<string> groups = new HashSet<string>();
				
				foreach (var item in Items) 
				{
					string group = item.Group;
					if (string.IsNullOrEmpty(group)) continue;
					
					if (groups.Contains(group)) continue;
					groups.Add(group);
					
					yield return group;
				}
			}
		}
		
		public void Connect(Session session)
		{
			Console.WriteLine(string.Format("Session start placeholder. Session {0} activated", session));
			
			if (session.Profile is Rdp.RdpProfile)
			{
				var rdpProfile = (Rdp.RdpProfile)session.Profile;
				
				string args = rdpProfile.ToCommandLineArguements(session.Server, session.Identity, false);
				string safeArgs = rdpProfile.ToCommandLineArguements(session.Server, session.Identity, true);
				
				new SessionHost("rdesktop", args, safeArgs);
			}	
			
			if (session.Profile is Vnc.VncProfile)
			{
				var vncProfile = (Vnc.VncProfile)session.Profile;
				
				string args = vncProfile.ToCommandLineArguements(session.Server, session.Identity, false);
				string safeArgs = vncProfile.ToCommandLineArguements(session.Server, session.Identity, true);
				
				new SessionHost("tight-vncviewer", args, safeArgs);
			}
			
			if (session.Profile is Ssh.SshProfile)
			{
				var sshProfile = (Ssh.SshProfile)session.Profile;
				
				string args = sshProfile.ToCommandLineArguements(session.Server, session.Identity);
				
				new SessionHostConsole("ssh", args, args, sshProfile.FullScreen);
			}			
		}
	}
}