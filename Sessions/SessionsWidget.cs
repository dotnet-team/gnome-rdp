// 
//  SessionsWidget.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

using Gtk;
using Gdk;

using GnomeRDP.Identies;
using GnomeRDP.Sessions;
using GnomeRDP.Logging;
using GnomeRDP.Profiles;
using GnomeRDP.Rdp;
using GnomeRDP.Ssh;
using GnomeRDP.Vnc;

namespace GnomeRDP.Sessions
{
	[ToolboxItem(true)]
	public partial class SessionsWidget : Bin
	{
		private Menu menu = new Menu();
		
		public SessionsWidget ()
		{
			this.Build ();
			
			Initialize();
		}
		
		private static class Columns
		{
			public const int item = 0;
			public const int icon = 1;
			public const int protocol = 2;
			public const int server = 3;
			public const int identity = 4;
			public const int profile = 5;
			
			public const int group = 2;
		}
		
		private void Initialize()
		{
			// Columns
			treeView.AppendColumn(new TreeViewColumn(null, new CellRendererPixbuf(), "pixbuf", Columns.icon));
			treeView.AppendColumn(new TreeViewColumn("Protocol", new CellRendererText(), "text", Columns.protocol));
			treeView.AppendColumn(new TreeViewColumn("Server", new CellRendererText(), "text", Columns.server));
			treeView.AppendColumn(new TreeViewColumn("Identity", new CellRendererText(), "text", Columns.identity));
			treeView.AppendColumn(new TreeViewColumn("Profile", new CellRendererText(), "text", Columns.profile));
			treeView.Selection.Mode = SelectionMode.Single;
			treeView.RowActivated += (s, e) => ConnectSelected();
			
			// Model
			TreeStore treeStore = new TreeStore(typeof(object), typeof(Pixbuf), typeof(string), typeof(string), typeof(string), typeof(string));
			treeStore.AppendValues(null, ResourceLoader.Find(ResourceLoader.Icons.folderSmall), Session.defaultGroupName);
			treeStore.SetSortColumnId(Columns.group, SortType.Ascending);
			treeStore.SetSortFunc(Columns.group, TreeIterCompareCallback);
			treeView.Model = treeStore;

			// Drag Drop
			TargetEntry[] targets = new TargetEntry[] { new TargetEntry("session", TargetFlags.Widget, 0) };
			treeView.EnableModelDragSource(ModifierType.Button1Mask, targets, DragAction.Copy);
			treeView.EnableModelDragDest(targets, DragAction.Private);
			treeView.DragDataReceived += TreeView_DragDataReceived;			
			
			// Tweaks
			treeView.EnableTreeLines = true;
			treeView.HeadersVisible = false;
			treeView.RulesHint = true;
			
			// Initialize Context Menu
			var propertiesMenuItem = new MenuItem("Properties");
			propertiesMenuItem.Activated += OnMenuPropertiesActivated;
			menu.Add(propertiesMenuItem);

			var deleteMenuItem = new MenuItem("Delete");
			deleteMenuItem.Activated += OnMenuDeleteActivated;			
			menu.Add(deleteMenuItem);		

			// Load data into the tree.
			Resync();
		}

		private void TreeView_DragDataReceived(object o, DragDataReceivedArgs args)
		{
			var selectedItems = GetSelectedItems();
			if (selectedItems.Length != 1) return;
			
			Session session = selectedItems[0] as Session;
			if (session == null) return;

			TreePath path;
			TreeViewDropPosition pos;
			treeView.GetDestRowAtPos(args.X, args.Y, out path, out pos);
							
			TreeStore treeStore = (TreeStore)treeView.Model;
			
			TreeIter iter;
			treeStore.GetIter(out iter, path);
			
			TreeIter groupIter;
			if (TryFindGroup(treeStore, iter, out groupIter) == false) return;

			session.Group = (string)treeStore.GetValue(groupIter, Columns.group);
			Program.SessionCollection.Save();
						
			Resync();
		}

		private static int TreeIterCompareCallback(TreeModel model, TreeIter leftIter, TreeIter rightIter)
		{
			TreeStore treeStore = (TreeStore)model;
		
			string left = null;
			TreeIter leftGroup;
			if (TryFindGroup(treeStore, leftIter, out leftGroup))
			{
				left = string.Format("{0} {1}", treeStore.GetValue(leftGroup, Columns.group), treeStore.GetValue(leftIter, Columns.server));
			}
			
			string right = null;
			TreeIter rightGroup;
			if (TryFindGroup(treeStore, rightIter, out rightGroup))
			{
				right = string.Format("{0} {1}", treeStore.GetValue(rightGroup, Columns.group), treeStore.GetValue(rightIter, Columns.server));
			}

			return string.Compare(left, right);		
		}
		
		private void Resync()
		{
			try
			{
				TreeStore treeStore = (TreeStore)treeView.Model;
				
				var deletedSessions = GetSessions(treeStore);
				foreach (var item in Program.SessionCollection.Items) 
				{
					deletedSessions.Remove(item);
					
					string group = item.Group;					
					TreeIter groupIter = FindGroup(string.IsNullOrEmpty(group) ? Session.defaultGroupName : group);
					
					object[] values = new object[] 
					{
						item, 
						Profile.GetIcon(item.Profile, true), 
						item.Profile == null ? null : item.Profile.Protocol.ToString(), 
						item.Server,
						item.Identity == null ? "<missing>" : item.Identity.ToString(),
						item.Profile == null ? "<missing>" : item.Profile.Description
					};
					
					TreeIter itemIter;
					if (TryFindSession(item, out itemIter) == false)
					{
						treeStore.AppendValues(groupIter, values);
					}
					else
					{
						TreeIter parent;
						if (treeStore.IterParent(out parent, itemIter) == false || treeStore.GetPath(parent).Compare(treeStore.GetPath(groupIter)) != 0)
						{
							treeStore.Remove(ref itemIter);
							treeStore.AppendValues(groupIter, values);
						}
						else
						{
							treeStore.SetValues(itemIter, values);
						}
					}
				}
				
				foreach (var session in deletedSessions) 
				{
					TreeIter iter;
					if (TryFindSession(session, out iter))
					{
						treeStore.Remove(ref iter);						
					}
				}
				
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
		}

		private TreeIter FindGroup(string value)
		{
			TreeStore treeStore = (TreeStore)treeView.Model;

			foreach (var iter in EnumerateChildren(treeStore)) 
			{
				string group = (string)treeStore.GetValue(iter, Columns.group);	
				if (group == value) return iter;
			}
						
			return treeStore.AppendValues(null, ResourceLoader.Find(ResourceLoader.Icons.folderSmall), value);
		}
		
		private static bool TryFindGroup(TreeStore treeStore, TreeIter iter, out TreeIter groupIter)
		{
			groupIter = iter;
			while (treeStore.GetValue(groupIter, Columns.item) as Session != null)
			{
				if (treeStore.IterParent(out groupIter, groupIter) == false )
				{
					groupIter = TreeIter.Zero;
					return false;
				}
			}
			
			return true;
		}
		
		private bool TryFindSession(Session item, out TreeIter iter)
		{
			TreeStore treeStore = (TreeStore)treeView.Model;

			foreach (var sessionIter in EnumerateSessions(treeStore))
			{
				var session = (Session)treeStore.GetValue(sessionIter, Columns.item);
				if (session.Id == item.Id)
				{
					iter = sessionIter;
					return true;
				}
			}
						
			iter = TreeIter.Zero;
			return false;
		}
		
		private static IEnumerable<TreeIter> EnumerateChildren(TreeStore treeStore)
		{
			TreeIter iter;
			if (treeStore.IterChildren(out iter) == false) yield break;
			
			do
			{
				yield return iter;		
			}
			while (treeStore.IterNext(ref iter));
		}
		
		private static IEnumerable<TreeIter> EnumerateChildren(TreeStore treeStore, TreeIter parent)
		{
			TreeIter iter;
			if (treeStore.IterChildren(out iter, parent) == false) yield break;
			
			do
			{
				yield return iter;		
			}
			while (treeStore.IterNext(ref iter));
		}
		
		private static HashSet<Session> GetSessions(TreeStore treeStore)
		{
			HashSet<Session> sessions = new HashSet<Session>();			
			foreach (var iter in EnumerateSessions(treeStore)) 
			{
				var session = (Session)treeStore.GetValue(iter, Columns.item);
				sessions.Add(session);
			}
			return sessions;
		}
		
		private static IEnumerable<TreeIter> EnumerateSessions(TreeStore treeStore)
		{
			foreach (var groupIter in EnumerateChildren(treeStore)) 
			{
				foreach (var sessionIter in EnumerateChildren(treeStore, groupIter)) 
				{
					var value = treeStore.GetValue(sessionIter, Columns.item);
					if (value is Session) yield return sessionIter;
				}
			}
		}
		
		private object[] GetSelectedItems()
		{
			List<object> items = new List<object>();
			try
			{
				foreach (var path in treeView.Selection.GetSelectedRows()) 
				{
					TreeIter iter;
					treeView.Model.GetIter(out iter, path);
					items.Add(treeView.Model.GetValue(iter, Columns.item));
				}
				return items.ToArray();
			}
			catch
			{
				return new Session[0];
			}
		}

		[GLib.ConnectBefore()]
		protected virtual void OnTreeViewButtonPressEvent (object o, Gtk.ButtonPressEventArgs args)
		{
			try
			{	
				if (args.Event.Button != 3) 
				{
					args.RetVal = false;
					return;
				}
				
				TreePath path;
				if (treeView.GetPathAtPos((int)args.Event.X, (int)args.Event.Y, out path) == false) return;
				
				treeView.GrabFocus();
				treeView.SetCursor(path, treeView.Columns[0], false);
				
				menu.ShowAll();
				menu.Popup();
				
				args.RetVal = true;
			}
			catch
			{
			}
		}

		protected virtual void OnMenuPropertiesActivated (object sender, System.EventArgs e)
		{
			try
			{
				foreach (var item in GetSelectedItems().Where(i => i is Session)) 
				{
					Session session = (Session)item;
					SessionDialog dlg = new SessionDialog(session);
					try
					{
						if (dlg.Run() == (int)ResponseType.Ok)
						{
							session.Server = dlg.Server;
							session.Identity = dlg.Identity;
							session.Profile = dlg.Profile;
							session.Group = dlg.Group;
							Program.SessionCollection.Save();
						}
					}
					catch(Exception ex)
					{
						Log.Add(ex);
					}
					finally
					{
						dlg.Destroy();
					}
				}
			}
			catch(Exception ex)
			{
				Log.Add(ex);
			}

			Resync();		
		}

		protected virtual void OnMenuDeleteActivated (object sender, System.EventArgs e)
		{
			try
			{
				HashSet<Session> sessions = new HashSet<Session>();
				foreach (var item in GetSelectedItems()) 
				{
					Session session = item as Session;
					if (session != null) sessions.Add(session);
				}
				if (sessions.Count == 0) return;
				
				MessageDialog dlg = new MessageDialog(null, DialogFlags.Modal, MessageType.Question, ButtonsType.YesNo, "Are you sure you want to delete these {0} item(s)?", sessions.Count);
				try
				{
					if(dlg.Run() != (int)ResponseType.Yes) return;

					foreach (var session in sessions) 
					{
						Program.SessionCollection.Remove(session);	
					}
				}
				finally
				{
					dlg.Destroy();
				}
			}
			catch(Exception ex)
			{
				Log.Add(ex);
			}
			
			Resync();
		}

		protected virtual void OnNewSessionActionActivated (object sender, System.EventArgs e)
		{
			SessionDialog dlg = new SessionDialog();
			try
			{
				if (dlg.Run() != (int)ResponseType.Ok) return;
				
				Session session = new Session(dlg.Server, dlg.Identity.Id, dlg.Profile.Id, dlg.Group);
				Program.SessionCollection.Add(session);
			}
			catch (Exception ex)
			{
				Log.Add(LogLevel.Error, ex.Message);
			}
			finally
			{
				dlg.Destroy();
			}
			
			Resync();
		}
	
		protected virtual void OnConnectActionActivated (object sender, System.EventArgs e)
		{
			ConnectSelected();
		}
		
		private void ConnectSelected()
		{
			try
			{
				foreach (var item in GetSelectedItems().Where(i => i is Session)) 
				{
					Program.SessionCollection.Connect((Session)item);
				}
			}
			catch(Exception ex)
			{
				Log.Add(ex);
			}
		}
	
		protected virtual void OnVisibilityNotifyEvent (object o, Gtk.VisibilityNotifyEventArgs args)
		{
			try
			{
				if (args.Event.State == VisibilityState.Unobscured)
				{
					Resync();
				}
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
		}
	}
}
