// 
//  SessionDialog.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Linq;

using Gtk;

using GnomeRDP.Profiles;
using GnomeRDP.Identies;

namespace GnomeRDP.Sessions
{
	public partial class SessionDialog : Dialog
	{
		public SessionDialog() : this(null)
		{
		}
		
		public SessionDialog(Session session)
		{
			this.Build ();

			var cellRenderer = new CellRendererText();

			cbProtocol.Model = new ListStore(typeof(Protocol), typeof(string));
			cbProtocol.PackStart(cellRenderer, true);
			cbProtocol.AddAttribute(cellRenderer, "text", 1);
			
			cbProfile.Model = new ListStore(typeof(Profile), typeof(string));
			cbProfile.PackStart(cellRenderer, true);
			cbProfile.AddAttribute(cellRenderer, "text", 1);

			cbIdentity.Model = new ListStore(typeof(Identity), typeof(string));
			cbIdentity.PackStart(cellRenderer, true);
			cbIdentity.AddAttribute(cellRenderer, "text", 1);			

			if (session == null)
			{
				SyncProtocolList(null);			
				SyncProfileList(null);						
				SyncIdentityList(null);
			}
			else
			{
				entryServer.Text = session.Server;
				entryGroup.Text = session.Group;
				
				SyncProtocolList(session.Profile);			
				SyncProfileList(session.Profile);						
				SyncIdentityList(session.Identity);
			}	
		}
		
		private void SyncProtocolList(Profile profile)
		{
			try
			{
				cbProtocol.Active = -1;
				
				var model = (ListStore)cbProtocol.Model;
				model.Clear();
				
				Protocol protocol = (profile == null) ? Protocol.rdp : profile.Protocol;
				
				foreach (Protocol item in Enum.GetValues(typeof(Protocol)))
				{
					var iter = model.AppendValues(item, item.ToString());
					if (item == protocol) cbProtocol.SetActiveIter(iter);
				}
			}
			catch
			{
			}
		}
		
		private void SyncProfileList(Profile profile)
		{
			try
			{
				cbProfile.Active = -1;
				
				var model = (ListStore)cbProfile.Model;
				model.Clear();
				
				TreeIter protocolIter;
				if (cbProtocol.GetActiveIter(out protocolIter) == false) return;
				
				var protocolModel = (ListStore)cbProtocol.Model;				
				var protocol = (Protocol)protocolModel.GetValue(protocolIter, 0);
				
				foreach (var item in Program.ProfileCollection.Items.Where((i) => i.Protocol == protocol)) 
				{
					var iter = model.AppendValues(item, item.Description);
					if (item == profile) cbProfile.SetActiveIter(iter);
				}
			}
			catch
			{
			}
		}

		private void SyncIdentityList(Identity identity)
		{
			try
			{
				cbIdentity.Active = -1;
				
				ListStore model = (ListStore)cbIdentity.Model;
				model.Clear();
				
				foreach (var item in Program.IdentityCollection.Items) 
				{
					var iter = model.AppendValues(item, item.ToString());
					if (item == identity) cbIdentity.SetActiveIter(iter);
				}
			}
			catch
			{
			}
		}
		
		protected virtual void OnCbProtocolChanged (object sender, System.EventArgs e)
		{
			SyncProfileList(null);
		}
		
		public string Server
		{
			get
			{
				return entryServer.Text;
			}
		}
				
		public Profile Profile
		{
			get
			{
				TreeIter iter;
				if (cbProfile.GetActiveIter(out iter) == false) return null;
				
				var model = (ListStore)cbProfile.Model;
				Profile profile = (Profile)model.GetValue(iter, 0);
				
				return profile;
			}
		}
		
		public Identity Identity
		{
			get
			{
				TreeIter iter;
				if (cbIdentity.GetActiveIter(out iter) == false) return null;
				
				ListStore model = (ListStore)cbIdentity.Model;
				Identity identity = (Identity)model.GetValue(iter, 0);
				
				return identity;
			}
		}
		
		public new string Group
		{
			get
			{
				return entryGroup.Text;
			}
		}
	}
}
