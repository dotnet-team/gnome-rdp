//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  Contributors
//  Stephen Phillips

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Threading;

using Gtk;

using GnomeRDP.Identies;
using GnomeRDP.Database;
using GnomeRDP.ApplicationDataFiles;
using GnomeRDP.Logging;
using GnomeRDP.Rdp;
using GnomeRDP.Profiles;
using GnomeRDP.Sessions;

namespace GnomeRDP
{
	public static class Program
	{
		[ThreadStatic]
		private static bool isMainThread;
		
		private static MainWindow mainWindow;
		
		private static void ShowVersion()
		{
			Console.WriteLine("GnomeRDP 3.0 alpha");
		}
		
		private static void ShowHelp()
		{
			ShowVersion();
			Console.WriteLine("Usage is mono gnome-rdp.exe [arguments]");
			Console.WriteLine();
			Console.WriteLine("    --help, -h          Displays this help message");
			Console.WriteLine("    --version, -v       Displays the program version");
			Console.WriteLine("    --start-hidden, -m  Starts GnomeRDP with the main window hidden");
			Console.WriteLine("    --databasepath      Old Sqlite configuration to import from");
			Console.WriteLine("    --configpath, -c    Override location to configuration folder");
		}
		
		public static void Main (string[] args)
		{
			bool startHidden;			
			ParseArgs (args, out startHidden);
			
			Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));

			Log.LogLevelFilter = LogLevel.Verbose;
			Log.Added += (sender, e) => Trace.WriteLine(string.Format("{0}: {1}", e.logLevel, e.message));
						
			FileManager.VerifyApplicationDataFolderExists();
		
			identityCollection = new IdentityCollection();
			profileCollection = new ProfileCollection();
			sessionCollection = new SessionCollection();
			
			if (sessionCollection.Count == 0) Sqlite.ImportSessions();
					
			isMainThread = true;
			
			Application.Init();
			
			mainWindow = new MainWindow();
			mainWindow.Show();
			if (startHidden) mainWindow.Visible = false;

			Application.Run();
			
			isClosed = true;
		}
		
		private static void ParseArgs (string[] args, out bool startHidden)
		{
			startHidden = false;
			
			// previousArg allows for arguments that come in pairs
			string previousArg = null;
			foreach (var arg in args) 
			{
				if (previousArg == null)
				{
					switch (arg)
					{
					case "--start-hidden":
					case "-m": startHidden = true; break;
					
					case "--version":
					case "-v": ShowVersion(); return;
					
					case "--help":
					case "-h": ShowHelp(); return;

					// In these cases, we just want to save the argument
					// because the next argument has the details we need.
					case "--databasepath": 
					case "--configpath":
					case "-c": previousArg = arg; break;
					}
				} 
				else
				{
					// Process the second of the pair of arguments
					switch (previousArg)
					{
						case "--databasepath": Sqlite.SetDatabasePath(arg); break;
						
						case "--configpath":
						case "-c": FileManager.SetApplicationDataFolderPath(arg); break;
					}
					previousArg = null;
				}
			}
		}
		
		private static SessionCollection sessionCollection;
		public static SessionCollection SessionCollection
		{
			get
			{
				return sessionCollection;
			}
		}
		
		private static ProfileCollection profileCollection;
		public static ProfileCollection ProfileCollection
		{
			get
			{
				return profileCollection;
			}
		}
		
		private static IdentityCollection identityCollection;
		public static IdentityCollection IdentityCollection
		{
			get
			{
				return identityCollection;
			}
		}
			
		public static string CreateID()
		{
    	    string id = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
			if (id.EndsWith("==")) id = id.Substring(0, id.Length - 2);
			return id;
		}
				
		public static void Invoke(System.Action action, TimeSpan timeout)
		{
			if (isMainThread)
			{
				action();
				return;
			}
			
			ManualResetEvent complete = new ManualResetEvent(false);
			GLib.Timeout.Add(0, () =>
			{
				action();
				complete.Set();
				return false;
			});
			
			if (complete.WaitOne(timeout) == false) throw new TimeoutException();
		}
		
		public static void ToggleMainWindowVisible()
		{
			GLib.Timeout.Add(0, () =>
			{
				mainWindow.Visible = !mainWindow.Visible;
				return false;
			});
		}
				
		public static void SetMainWindowVisible(bool visible)
		{
			GLib.Timeout.Add(0, () =>
			{
				mainWindow.Visible = visible;
				return false;
			});
		}
				
		private static bool isClosed = false;
		public static bool IsClosed
		{
			get
			{
				return isClosed;
			}
		}
	}
}