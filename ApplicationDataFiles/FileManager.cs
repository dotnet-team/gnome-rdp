//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//  Contributors
//  Stephen Phillips

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Diagnostics;

using GnomeRDP.Identies;
using GnomeRDP.Logging;
using GnomeRDP.Rdp;
using GnomeRDP.Vnc;
using GnomeRDP.Ssh;
using GnomeRDP.Sessions;

namespace GnomeRDP.ApplicationDataFiles
{
	public static class FileManager
	{
		private const string nullValue = "<null>";
		
		private static class Files
		{
			public const string gnomeRdp = "gnome-rdp.conf";
			public const string indentities = "identities.conf";
			public const string rdpProfiles = "rdpProfiles.conf";
			public const string vncProfiles = "vncProfiles.conf";
			public const string sshProfiles = "sshProfiles.conf";
			public const string sessions = "sessions.conf";

			private static string applicationDataFolderPath = GetDefaultApplicationDataFolderPath();
						
			public static string GetPathTo(string file)
			{
				return string.Format("{0}{1}{2}", GetApplicationDataFolderPath(), Path.DirectorySeparatorChar, file);
			}
			
			public static void SetApplicationDataFolderPath(string path)
			{
				applicationDataFolderPath = path;
			}

			private static string GetDefaultApplicationDataFolderPath()
			{
				return string.Format("{0}{1}{2}", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Path.DirectorySeparatorChar, "GnomeRDP");
			}
			
			public static string GetApplicationDataFolderPath()
			{
				return applicationDataFolderPath;
			}
		}

		private static class ItemKeys
		{
			public const string identity = "[Identity]";
			public const string rdpProfile = "[RdpProfile]";
			public const string vncProfile = "[VncProfile]";
			public const string sshProfile = "[SshProfile]";
			public const string session = "[Session]";
		}
		
		public static void SetApplicationDataFolderPath(string path)
		{
			Files.SetApplicationDataFolderPath(path);
		}

		public static void VerifyApplicationDataFolderExists()
		{
			string path = Files.GetApplicationDataFolderPath();
			
			if (Directory.Exists(path)) return;
			
			Directory.CreateDirectory(path);
		}

		public static Identity[] LoadIdentities()
		{
			return LoadObjects<Identity>(Files.indentities, ItemKeys.identity, Identity.Create);
		}
		
		public static void SaveIdentities(IEnumerable<Identity> identities)
		{
			SaveObjects<Identity>(identities, Files.indentities, ItemKeys.identity, Identity.ToDictionary);
		}
		
		public static RdpProfile[] LoadRdpProfiles()
		{
			return LoadObjects<RdpProfile>(Files.rdpProfiles, ItemKeys.rdpProfile, RdpProfile.Create);
		}
		
		public static void SaveRdpProfiles(IEnumerable<RdpProfile> rdpProfiles)
		{
			SaveObjects<RdpProfile>(rdpProfiles, Files.rdpProfiles, ItemKeys.rdpProfile, RdpProfile.ToDictionary);
		}
		
		public static VncProfile[] LoadVncProfiles()
		{
			return LoadObjects<VncProfile>(Files.vncProfiles, ItemKeys.vncProfile, VncProfile.Create);
		}

		public static void SaveVncProfiles(IEnumerable<VncProfile> vncProfiles)
		{
			SaveObjects<VncProfile>(vncProfiles, Files.vncProfiles, ItemKeys.vncProfile, VncProfile.ToDictionary);
		}
		
		public static SshProfile[] LoadSshProfiles()
		{
			return LoadObjects<SshProfile>(Files.sshProfiles, ItemKeys.sshProfile, SshProfile.Create);
		}

		public static void SaveSshProfiles(IEnumerable<SshProfile> sshProfiles)
		{
			SaveObjects<SshProfile>(sshProfiles, Files.sshProfiles, ItemKeys.sshProfile, SshProfile.ToDictionary);
		}
		
		public static Session[] LoadSessions()
		{
			return LoadObjects<Session>(Files.sessions, ItemKeys.session, Session.Create);
		}
		
		public static void SaveSessions(IEnumerable<Session> sessions)
		{
			SaveObjects<Session>(sessions, Files.sessions, ItemKeys.session, Session.ToDictionary);
		}
		
		private static T[] LoadObjects<T>(string filename, string itemKey, Func<Dictionary<string, string>, T> CreateDelegate)
		{
			List<T> items = new List<T>();
				
			try
			{
				string path = Files.GetPathTo(filename);			
				using (StreamReader r = File.OpenText(path))
				{
					Dictionary<string, string> dictionary = new Dictionary<string, string>();
					
					string line;
					while ((line = r.ReadLine()) != null)
					{
						if (line == itemKey)
						{
							if (dictionary.ContainsKey(itemKey))
							{
								// Create item from existing dictionary before starting a new one.
								try
								{
									items.Add(CreateDelegate(dictionary));
								}
								catch (Exception ex)
								{
									Log.Add(LogLevel.Error, "Error in file {0} deserializing object.", filename);
									Log.Add(ex);
								}						
							}

							// clear the dictionary so that it can be used for the next item.
							dictionary.Clear();
							dictionary[itemKey] = null;
							continue;
						}
						
						int index = line.IndexOf('=');
						if (index == -1) continue;
						
						string key = line.Substring(0, index).Trim();
						string value = line.Substring(index + 1).Trim();
						
						if (value == nullValue) 
						{
							value = null;
						}
						else if (value.StartsWith("\"") && value.EndsWith("\""))
						{
							value = value.Substring(1,value.Length - 2);
						}
						
						dictionary[key] = value;
					}
					
					if (dictionary.ContainsKey(itemKey))
					{
						// Get the last entry.
						try
						{
							items.Add(CreateDelegate(dictionary));
						}
						catch (Exception ex)
						{
							Log.Add(LogLevel.Error, "Error in file {0} deserializing object. {1}", filename, ex.Message);
						}						
					}
				}
			}
			catch (Exception ex)
			{
				Log.Add(LogLevel.Error, "Error in file {0} loading objects. {1}", filename, ex.Message);
			}
				
			return items.ToArray();
		}
		
		private static void SaveObjects<T>(IEnumerable<T> items, string filename, string itemKey, Func<T, Dictionary<string, string>> ToDictionaryDelegate)
		{
			string filePath = Files.GetPathTo(filename);
			
			string randomPath = Files.GetPathTo(Path.GetRandomFileName());
			try
			{
				using (StreamWriter w = File.CreateText(randomPath))
				{
					foreach (var item in items) 
					{
						Dictionary<string, string> dictionary = ToDictionaryDelegate(item);
						
						w.WriteLine(itemKey);
						foreach (var pair in dictionary) 
						{
							if (pair.Value == null)
							{
								w.WriteLine(string.Format("{0}={1}", pair.Key, nullValue));	
							}
							else
							{
								w.WriteLine(string.Format("{0}=\"{1}\"", pair.Key, pair.Value));	
							}
						}
						w.WriteLine();
					}
				}

				File.Copy(randomPath, filePath, true);
			}
			finally
			{
				File.Delete(randomPath);
			}
		}		
	}
}
