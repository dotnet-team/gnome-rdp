// 
//  Collection.cs
//  
//  Author:
//       James P Michels III <james.p.michels@gmail.com>
// 
//  Copyright (c) 2009 James P Michels III
// 
//  This library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as
//  published by the Free Software Foundation; either version 2.1 of the
//  License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful, but
//  WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//  Lesser General Public License for more details.
// 
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

using System;
using System.Collections.Generic;

namespace GnomeRDP
{
	public abstract class Collection<T> where T : class, IIdType, IEquatable<T>
	{
		protected readonly Dictionary<string, T> items = new Dictionary<string, T>();

		public Collection()
		{
			T[] items = LoadItems();
			
			foreach (var item in items) 
			{
				this.items[item.Id] = item;	
			}
		}
	
		protected abstract T[] LoadItems();
		
		public void Add(T item)
		{
			lock(this.items)
			{
				this.items[item.Id] = item;
			}
			Save();
		}
		
		public void Remove(T item)
		{
			lock(this.items)
			{
				this.items.Remove(item.Id);
			}
			Save();
		}
		
		public T Find(string id)
		{
			lock(this.items)
			{
				T item;
				this.items.TryGetValue(id, out item);
				return item;
			}
		}
		
		public T[] ToArray()
		{
			lock(this.items)
			{
				T[] array = new T[this.items.Values.Count];
				this.items.Values.CopyTo(array, 0);
				return array;
			}
		}
		
		public int Count
		{
			get
			{
				lock (this.items)
				{
					return this.items.Count;
				}
			}
		}
		
		public IEnumerable<T> Items
		{
			get
			{
				T[] items = ToArray();
				
				foreach (var item in items) 
				{
					yield return item;	
				}
			}
		}
				
		// HACK workaround for mono 2.4.x compiler bug. remove after moving all deployments to mono 2.6 or higher.
		public T FindSimiliarOrAdd(T t)
		{
			ISimiliar<T> u = (ISimiliar<T>) t;
			
			foreach (var item in ToArray()) 
			{
				if (u.IsLike(item)) return item;
			}
			
			Add(t);
			
			return t;
		}

//		public U FindSimiliarOrAdd<U>(U u) where U : T ,ISimiliar<T>
//		{
//			foreach (var item in ToArray()) 
//			{
//				if (u.IsLike(item)) return (U)item;
//			}
//			
//			Add(u);
//			
//			return u;
//		}

		public abstract void Save();
	}
}
