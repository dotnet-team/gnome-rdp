//  
//  Copyright (C) 2009 James P Michels III
// 
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
// 
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
// 
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using Gnome.Keyring;
using Mono.Unix;

using GnomeRDP.Logging;

namespace GnomeRDP
{
    /// <summary>
    /// Generic class to manage the GNOME keyring.
    /// </summary>
	public static class KeyringProxy
	{		
		private const string objectName = "Gnome-RDP";

		/// <summary>
		/// Gets a password from the GNOME keyring.
		/// </summary>
		/// <returns>
		/// Returns the password from the GNOME keyring. If no password is found, returns null.
		/// </returns>
		public static string GetPassword(string user, string domain, string server, Protocol protocol)
		{
			try
			{
				string defaultKeyring = Ring.GetDefaultKeyring();
				
				string obj = objectName;
				string authType = null;
				int port = 0;
				
				NetItemData[] items = Ring.FindNetworkPassword(user, domain, server, obj, protocol.ToString(), authType, port);
				if(items == null) return null;
				
				string password = null;
				foreach(NetItemData item in items)
				{
					if(item.Keyring == defaultKeyring && password == null)
					{
						password = item.Secret;
					}
					else
					{
						Log.Add(LogLevel.Warning, Catalog.GetString("Duplicate entries found. Deleting item {0} from keyring {1}"), item.ItemID, item.Keyring);
						Ring.DeleteItem(item.Keyring, item.ItemID);
					}
				}

				return password;
			}
			catch (Exception)
			{
				Log.Add(LogLevel.Error, "Error retreiving password for {0}\\{1} at {2}:{3}", domain, user, server, protocol);
				return null;
			}
		}
				
		/// <summary>
		/// Stores a password in the gnome keyring.
		/// </summary>
		public static void SetPassword(string user, string domain, string server, Protocol protocol, string password)
		{
			try
			{
				string defaultKeyring = Ring.GetDefaultKeyring();
				string obj = objectName;
				string authType = null;
				int port = 0;

				Ring.CreateOrModifyNetworkPassword(defaultKeyring, user, domain, server, obj, protocol.ToString(), authType, port, password);
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
		}
		
		public static void ClearSavedPasswords(string user, string domain)
		{
			try
			{
				string defaultKeyring = Ring.GetDefaultKeyring();
				
				var attributes = new Hashtable();				
				attributes["object"] = objectName;
				attributes["user"] = user;
				attributes["domain"] = domain;
				
				foreach (var itemData in Ring.Find(ItemType.NetworkPassword, attributes))
				{
					if (itemData.Keyring == defaultKeyring) Ring.DeleteItem(defaultKeyring, itemData.ItemID);
				}
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
		}

		public static void ReplaceSavedPasswords(string user, string domain, string password)
		{
			try
			{
				string defaultKeyring = Ring.GetDefaultKeyring();
				
				var attributes = new Hashtable();
				attributes["object"] = objectName;
				attributes["user"] = user;
				attributes["domain"] = domain;
				
				foreach (var itemData in Ring.Find(ItemType.NetworkPassword, attributes))
				{
					var netItemData = (NetItemData)itemData;
					
					string server = netItemData.Server;
					string protocol = netItemData.Protocol;
					string authType = null;
					int port = 0;
	
					Ring.CreateOrModifyNetworkPassword(defaultKeyring, user, domain, server, objectName, protocol, authType, port, password);
				}
			}
			catch (Exception ex)
			{
				Log.Add(ex);
			}
		}
	}
}
